import * as React from 'react'
import Main from '../src/components/MainContainer'
import PageExhibitions from '../src/components/PageExhibitions'
import PageGallery from '../src/components/PageGallery'
import PageInfos from '../src/components/PageInfos'
import PageStandard from '../src/components/PageStandard'
import { Page, PageType } from '../src/entities/page'
import { Language } from '../src/server/config'
import { Entities } from '../src/server/content'
import { getEntityProps, getLanguage } from '../src/server/page-helpers'

interface Props {
	language: Language
	page: Page
	entities: Entities
	path: string
}

class Index extends React.Component<Props, null> {
	static async getInitialProps(args): Promise<Props> {
		const entities = await getEntityProps(args)
		const props = {
			entities,
			language: getLanguage(args),
			page: entities.pages[args.query.id],
			path: args.asPath,
		}
		return props
	}

	getPageComponent() {
		const p = this.props.page

		switch (p.type) {
			case PageType.Gallery:
			case PageType.Editions:
			case PageType.Cartoon:
				return <PageGallery page={p} workIds={p.articles} />

			case PageType.Exhibitions:
				return <PageExhibitions page={p} />

			case PageType.Events:
			case PageType.Statements:
			case PageType.News:
				return (
					<PageInfos
						page={p}
						language={this.props.language}
						entities={this.props.entities}
					/>
				)

			default:
				return <PageStandard page={p} language={this.props.language} />
		}
	}

	render() {
		return <Main {...this.props}>{this.getPageComponent()}</Main>
	}
}

export default Index
