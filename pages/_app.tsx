import Router from 'next/router'
import { useEffect } from 'react'

function MyApp({ Component, pageProps }) {
	useEffect(() => {
		if ('scrollRestoration' in window.history) {
			window.history.scrollRestoration = 'manual'
			let lastScroll: number | false | undefined
			const cachedScrollPositions: number[] = []

			Router.events.on('routeChangeStart', (pathname: string) => {
				if (
					Router.pathname.indexOf('artwork') >= 0 &&
					pathname.indexOf('artwork') >= 0 &&
					pathname.indexOf('artwork') < 10
				) {
					// console.log('inside of artwork!')
					lastScroll = window.scrollY
				} else if (lastScroll === false) {
					cachedScrollPositions.push(window.scrollY)
				}
				// console.log(
				// 	'onRoutrChangeStart',
				// 	lastScroll,
				// 	cachedScrollPositions,
				// 	pathname,
				// 	Router.pathname,
				// )
			})

			Router.events.on('routeChangeComplete', () => {
				// console.log('onRoutrChangeComplete', lastScroll, cachedScrollPositions)
				requestAnimationFrame(() => {
					window.scrollTo(0, lastScroll || 0)
					lastScroll = false
				})
			})

			Router.beforePopState(() => {
				lastScroll = cachedScrollPositions.pop()
				// console.log('beforePopState', lastScroll, cachedScrollPositions)
				return true
			})
		}
	}, [])
	return <Component {...pageProps} />
}

export default MyApp
