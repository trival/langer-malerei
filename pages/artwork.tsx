import Router from 'next/router'
import React from 'react'
import Slick from 'react-slick'
import ExhibitionLink from '../src/components/Exhibition'
import { ZoomIn } from '../src/components/icons/ZoomIn'
import ImgDetails from '../src/components/ImgDetails'
import Main from '../src/components/MainContainer'
import Markdown from '../src/components/MarkdownContent'
import { Article } from '../src/entities/article'
import { Exhibition } from '../src/entities/exhibition'
import { FixedPageIds, Page } from '../src/entities/page'
import { TranslationIds } from '../src/entities/translation'
import { Language } from '../src/server/config'
import { Entities } from '../src/server/content'
import { getEntityProps, getLanguage } from '../src/server/page-helpers'
import { preventAndStop } from '../src/utils/react'
import { $t } from '../src/utils/string'
import { isImage, toPath } from '../src/utils/url'

interface GalleryItem {
	img: string
	index: number
	id: string
	work?: Article
}

interface Props {
	language: Language
	page?: Page
	exhibition?: Exhibition
	entities: Entities
	id: string
	path: string
}

interface State {
	currentIndex: number
	startIndex: number
	detailsOpen: boolean
}

class Index extends React.Component<Props, State> {
	static async getInitialProps(args): Promise<Props> {
		const entities = await getEntityProps(args)
		const props = {
			entities,
			language: getLanguage(args),
			page: entities.pages[args.query.page],
			exhibition: entities.exhibitions[args.query.exhibition],
			id: args.query.id,
			path: args.asPath,
		}
		return props
	}

	gallery: GalleryItem[] = []
	slider: any = null

	constructor(props) {
		super(props)
		this.createGallery()
		const index = this.gallery.findIndex((i) => i.id === this.props.id)
		this.state = {
			currentIndex: index,
			startIndex: index,
			detailsOpen: false,
		}
	}

	createGallery() {
		let i = 0
		const processWork = (work) => {
			for (const img of work.imgs) {
				this.gallery.push({
					index: i++,
					id: work.id,
					work,
					img,
				})
			}
		}
		const arts = this.props.entities.articles

		if (this.props.exhibition && this.props.exhibition.works) {
			this.props.exhibition.works.forEach((workId) => {
				const work = arts[workId]
				work && processWork(work)
			})
		} else if (this.props.page) {
			if (this.props.page.parentId !== FixedPageIds.StartPage) {
				this.props.page.imgs.filter(isImage).forEach((img) => {
					this.gallery.push({ id: 'p' + i, index: i++, img })
				})
			}

			this.props.page.articles.forEach((workId) => {
				const work = arts[workId]
				work && processWork(work)
			})
		} else if (arts[this.props.id]) {
			processWork(arts[this.props.id])
		}
	}

	preload(index) {
		const length = this.gallery.length - 1
		const next = (index + 1) % length
		const prev = (index + length - 1) % length

		const prevItem = this.gallery[prev]
		if (prevItem && prevItem.img) {
			const prevImg = new Image()
			// prevImg.onload = () => console.log('loaded', prevItem.img)
			prevImg.src = prevItem.img
		}

		const nextItem = this.gallery[next]
		if (nextItem && nextItem.img) {
			const nextImg = new Image()
			// nextImg.onload = () => console.log('loaded', nextItem.img)
			nextImg.src = nextItem.img
		}
	}

	render() {
		const lang = this.props.language
		const item: GalleryItem | undefined = this.gallery[this.state.currentIndex]
		const work = item && item.work
		const exs = work && work.exhibitions
		const translations = this.props.entities.translations

		const onSliderChange = (old, index) => {
			this.preload(index)
			// timeout to prevent weird slider behavior
			setTimeout(() => {
				this.setState((state) => ({
					...state,
					currentIndex: index,
					detailsOpen: false,
				}))
				const query = { ...Router.query, id: this.gallery[index].id }
				Router.replace(
					{ pathname: Router.pathname, query },
					toPath({
						entities: this.props.entities,
						...query,
						path: 'artwork',
					} as any),
				)
			}, 200)
		}

		const nextClick = () => this.slider && this.slider.slickNext()
		const prevClick = () => this.slider && this.slider.slickPrev()
		const backClick = () => {
			Router.back()
		}

		const workInfos: string[] = []
		if (work) {
			if (work.technique) {
				workInfos.push($t(translations[work.technique], lang))
			}
			if (work.ground) {
				workInfos.push($t(translations[work.ground], lang))
			}
			if (work.widthInner || work.widthOuter) {
				workInfos.push(
					(work.widthInner || work.widthOuter) +
						' x ' +
						(work.heightInner || work.heightOuter) +
						' mm',
				)
			}
			if (work.paper) {
				workInfos.push(
					$t(translations[TranslationIds.paper], lang) + ': ' + work.paper,
				)
				if (work.paperWeight) {
					workInfos.push(work.paperWeight + ' g/qm')
				}
			}
			if (work.edition) {
				workInfos.push(
					$t(translations[TranslationIds.edition], lang) + ': ' + work.edition,
				)
			}
		}
		const workInfo = workInfos.join(', ')

		const menuPage = this.props.page
			? this.props.page.parentId === FixedPageIds.StartPage
				? this.props.page
				: this.props.entities.pages[this.props.page.parentId]
			: this.props.entities.pages[FixedPageIds.Exhibitions]

		return (
			<Main {...this.props} page={menuPage} meta={work}>
				<article className="work-details">
					<header>
						{item && this.gallery.length === 1 && (
							<nav>
								<span>
									{item.index + 1} / {this.gallery.length}
								</span>
								<button
									className="zoom-in"
									onClick={() =>
										this.setState((s) => ({ ...s, detailsOpen: true }))
									}
								>
									<ZoomIn />
								</button>
								<a className="gallery-back" onClick={backClick}>
									{$t(translations[TranslationIds.back], lang)}
								</a>
							</nav>
						)}
						{item && this.gallery.length > 1 && (
							<nav>
								<button className="gallery-prev" onClick={prevClick}>
									<img src="/static/carousel_arrow_left.png" alt="prev" />
								</button>
								<span>
									{item.index + 1} / {this.gallery.length}
								</span>
								<button
									className="zoom-in"
									onClick={() =>
										this.setState((s) => ({ ...s, detailsOpen: true }))
									}
								>
									<ZoomIn />
								</button>
								<a className="gallery-back" onClick={backClick}>
									{$t(translations[TranslationIds.back], lang)}
								</a>
								<button className="gallery-next" onClick={nextClick}>
									<img src="/static/carousel_arrow_right.png" alt="next" />
								</button>
							</nav>
						)}
						{work && (
							<h2>
								{$t(work.name, lang) ||
									$t(translations[TranslationIds.untitled], lang)}{' '}
								<small>({new Date(work.date as string).getFullYear()})</small>
							</h2>
						)}
					</header>

					{!!item && (
						<section className="image" onDragStart={preventAndStop}>
							{this.gallery.length > 1 ? (
								<Slick
									adaptiveHeight
									arrows
									useCSS
									accessibility
									lazyLoad="ondemand"
									initialSlide={this.state.startIndex}
									beforeChange={onSliderChange}
									ref={(slider) => (this.slider = slider)}
									key={'slider' + this.state.startIndex}
								>
									{this.gallery.map((item) => (
										<figure key={item.index}>
											<img
												src={item.img}
												alt={item.work && $t(item.work.altText, lang)}
											/>
										</figure>
									))}
								</Slick>
							) : (
								<figure>
									<img
										src={item.img}
										alt={item.work && $t(item.work.altText, lang)}
									/>
								</figure>
							)}
							<p className="download-img">
								<a href={item.img} download>
									Download
								</a>
							</p>
						</section>
					)}

					{workInfo && (
						<section className="info">
							<p>{workInfo}</p>
							{work && work.publicPrice && work.price && (
								<p>
									{$t(translations[TranslationIds.price], lang)}: {work.price} €
								</p>
							)}
						</section>
					)}

					{work && work.description && $t(work.description, lang) && (
						<section className="description">
							<Markdown markdown={$t(work.description, lang)} />
						</section>
					)}

					{exs &&
						exs.length > 0 && [
							<h4 key="foo">
								{$t(translations[TranslationIds.exhibitions], lang)}
							</h4>,
							<section className="collection" key="bar">
								{exs.map((eid) => {
									const e = this.props.entities.exhibitions[eid]
									return (
										<ExhibitionLink
											short
											exhibition={e}
											key={'we' + eid + (work && work.id)}
											language={lang}
										/>
									)
								})}
							</section>,
						]}

					{!!item && this.state.detailsOpen && (
						<ImgDetails
							imgUrl={item.img}
							onClose={() =>
								this.setState((s) => ({ ...s, detailsOpen: false }))
							}
						/>
					)}
				</article>
			</Main>
		)
	}
}

export default Index
