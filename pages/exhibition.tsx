import * as React from 'react'
import Link from '../src/components/Link'
import Main from '../src/components/MainContainer'
import MarkDown from '../src/components/MarkdownContent'
import Thumb from '../src/components/Thumbnail'
import { Exhibition } from '../src/entities/exhibition'
import { FixedPageIds } from '../src/entities/page'
import { TranslationIds } from '../src/entities/translation'
import { Language } from '../src/server/config'
import { Entities } from '../src/server/content'
import { getEntityProps, getLanguage } from '../src/server/page-helpers'
import { formatDateDEShort } from '../src/utils/date'
import { $t } from '../src/utils/string'

interface Props {
	language: Language
	exhibition: Exhibition
	entities: Entities
	path: string
}

class ExhibitionPage extends React.Component<Props, null> {
	static async getInitialProps(args): Promise<Props> {
		const entities = await getEntityProps(args)
		const props = {
			entities,
			language: getLanguage(args),
			exhibition: entities.exhibitions[args.query.id],
			path: args.asPath,
		}
		return props
	}

	render() {
		const { language: lang, entities } = this.props
		const e = this.props.exhibition
		const p = entities.pages[FixedPageIds.Exhibitions]
		const translations = entities.translations
		return (
			<Main {...this.props} page={p}>
				<article>
					{e && (
						<>
							<header>
								<h2>
									{e.name} <small>({e.year})</small>
								</h2>
							</header>

							<table className="exhibition-data">
								<tbody>
									{e.place && (
										<tr>
											<td>{$t(translations[TranslationIds.place], lang)}:</td>
											<td>{e.place}</td>
										</tr>
									)}
									{!e.hideGallery && e.gallery && (
										<tr>
											<td>{$t(translations[TranslationIds.gallery], lang)}:</td>
											<td>
												<MarkDown markdown={e.gallery} />
											</td>
										</tr>
									)}
									{(e.startDate || e.dateEstimate) && (
										<tr>
											<td>
												{$t(translations[TranslationIds.duration], lang)}:
											</td>
											<td>
												{(e.startDate && formatDateDEShort(e.startDate)) ||
													e.dateEstimate}
												{e.endDate && ' - ' + formatDateDEShort(e.endDate)}
											</td>
										</tr>
									)}
								</tbody>
							</table>

							{e.works && e.works.length > 0 && (
								<section className="collection">
									{e.works.map((id) => {
										const work = entities.articles[id]
										return (
											<Link
												path="artwork"
												id={id}
												query={{ exhibition: e.id }}
												key={'work' + work.id}
											>
												<a className="preview-link work-preview">
													<Thumb
														src={work.imgPreview}
														alt={$t(work.metaTitle, lang)}
													/>
												</a>
											</Link>
										)
									})}
								</section>
							)}
						</>
					)}

					{p && (
						<section className="text-content">
							<MarkDown markdown={$t(p.text, lang)} />
						</section>
					)}
				</article>
			</Main>
		)
	}
}

export default ExhibitionPage
