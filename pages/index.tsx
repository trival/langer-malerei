import cn from 'classnames'
import * as React from 'react'
import Main from '../src/components/MainContainer'
import PagePreviewLink from '../src/components/PagePreviewLink'
import { FixedPageIds } from '../src/entities/page'
import { Language } from '../src/server/config'
import { Entities } from '../src/server/content'
import { getEntityProps, getLanguage } from '../src/server/page-helpers'

interface Props {
	language: Language
	entities: Entities
	path: string
}

interface State {
	isVerticalLayout: boolean
	visible: boolean
}

class Index extends React.Component<Props, State> {
	static async getInitialProps(args): Promise<Props> {
		const entities = await getEntityProps(args)
		const props = {
			entities,
			language: getLanguage(args),
			path: args.asPath,
		}
		return props
	}

	window = typeof window !== 'undefined' ? window : null

	constructor(props) {
		super(props)
		this.state = {
			isVerticalLayout: true,
			visible: false,
		}
	}

	widthListener = () => {
		const isVerticalLayout = !(this.window && this.window.innerWidth >= 760)
		if (isVerticalLayout !== this.state.isVerticalLayout) {
			this.setState(state => ({ ...state, isVerticalLayout }))
		}
	}

	componentDidMount() {
		this.widthListener()
		this.window && this.window.addEventListener('resize', this.widthListener)
		this.setState(state => ({ ...state, visible: true }))
	}

	componentWillUnmount() {
		this.window && this.window.removeEventListener('resize', this.widthListener)
	}

	render() {
		const index = this.props.entities.pages[FixedPageIds.StartPage]
		const statements = this.props.entities.pages[FixedPageIds.Statements]
		const cartoon = this.props.entities.pages[FixedPageIds.Cartoon]
		const editions = this.props.entities.pages[FixedPageIds.Editions]
		const gallery = this.props.entities.pages[FixedPageIds.Gallery]
		const news = this.props.entities.pages[FixedPageIds.News]
		const exhibitions = this.props.entities.pages[FixedPageIds.Exhibitions]
		const literature = this.props.entities.pages[FixedPageIds.Literature]
		const videos = this.props.entities.pages[FixedPageIds.Videos]

		const lang = this.props.language
		const className = cn('featured-pages', {
			visible: this.state.visible,
		})

		const galleryImgs = [1, 2, 3, 4, 5, 6]
			.map(
				() =>
					gallery.articles[Math.floor(Math.random() * gallery.articles.length)],
			)
			.map(artId => this.props.entities.articles[artId]?.imgPreview)

		const statmentImgs = statements.children
			.map(pid => {
				const page = this.props.entities.pages[pid]
				if (page.articles && page.articles.length > 0) {
					const aid =
						page.articles[Math.floor(Math.random() * page.articles.length)]
					return this.props.entities.articles[aid]?.imgPreview
				}
				if (page.imgs && page.imgs.length > 0) {
					return page.imgs[Math.floor(Math.random() * page.imgs.length)]
				}
				return ''
			})
			.filter(s => s)

		return (
			<Main {...this.props} page={index} hideMenu>
				{this.state.isVerticalLayout ? (
					<nav className={className} key="vertical">
						<div className="container-2-3" key="container-2-3">
							<PagePreviewLink
								page={statements}
								imgs={statmentImgs}
								language={lang}
								big
							/>
							<PagePreviewLink page={editions} language={lang} />
							<div className="container-1-2">
								<PagePreviewLink page={videos} language={lang} />
								<PagePreviewLink page={news} language={lang} />
							</div>
							<PagePreviewLink page={exhibitions} language={lang} big />
						</div>
						<div className="container-1-3" key="container-1-3">
							<PagePreviewLink
								page={gallery}
								imgs={galleryImgs}
								language={lang}
							/>
							<PagePreviewLink page={literature} language={lang} />
							<PagePreviewLink page={cartoon} language={lang} />
						</div>
					</nav>
				) : (
					<nav className={className} key="horizontal">
						<div className="container-2-5" key="container-2-5-1">
							<PagePreviewLink
								page={statements}
								imgs={statmentImgs}
								language={lang}
								big
							/>
							<PagePreviewLink page={cartoon} language={lang} />
							<PagePreviewLink page={videos} language={lang} />
						</div>
						<div className="container-2-5" key="container-2-5-2">
							<PagePreviewLink page={editions} language={lang} />
							<div className="container-1-2">
								<PagePreviewLink page={news} language={lang} />
								<PagePreviewLink page={literature} language={lang} />
							</div>
							<PagePreviewLink page={exhibitions} language={lang} big />
						</div>
						<div className="container-1-5" key="container-1-5">
							<PagePreviewLink
								page={gallery}
								imgs={galleryImgs}
								language={lang}
							/>
						</div>
					</nav>
				)}
			</Main>
		)
	}
}

export default Index
