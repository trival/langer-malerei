import { Gulp } from 'gulp'

const gulp: Gulp = require('gulp')
const autoprefixer = require('gulp-autoprefixer')
const stylus = require('gulp-stylus')
const livereload = require('gulp-livereload')
const gulpMocha = require('gulp-mocha')
const nodemon = require('gulp-nodemon')
const lost = require('lost')
const postcss = require('gulp-postcss')

const paths = {
	stylesRoot: 'style/styles.styl',
	styles: 'style/**/*',
	distStatic: 'static/',
	stylesBuild: 'static/styles.css',
	tests: ['test/test.ts', 'test/**/*-test.ts'],
	srcCode: ['src/**/*.ts', 'src/**/*.tsx'],
}

const styles = () =>
	gulp
		.src(paths.stylesRoot)
		// .pipe(sourcemaps.init())
		.pipe(stylus())
		.on('error', function (e) {
			console.log(e)
			// @ts-ignore
			this.emit('end')
		})
		.pipe(postcss([lost]))
		.pipe(autoprefixer())
		// .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.distStatic))

const testOnce = () =>
	gulp
		.src(paths.tests)
		.pipe(
			gulpMocha({
				require: ['ts-node/register'],
			}),
		)
		.on('error', function (e) {
			if (/test/.test(e.message)) {
				console.log(e.message)
			} else {
				console.error(e.stack)
			}
			// @ts-ignore
			this.emit('end')
		})

const testWatch = () => gulp.watch(paths.srcCode.concat(paths.tests), testOnce)

const testTask = gulp.series(testOnce, testWatch)

const watch = gulp.series(styles, () => {
	livereload.listen()
	gulp.watch(paths.styles, styles)

	const styleReload = gulp.watch(paths.stylesBuild)
	styleReload.on('change', (path) => livereload.changed(path))

	return nodemon({
		script: 'start-server.js',
		exec: 'node -r dotenv/config',
		watch: ['src/entities/', 'src/server/', 'src/utils/'],
		ext: 'ts',
		delay: 2500,
	})
})

module.exports = {
	default: watch,
	test: testTask,
	'test-once': testOnce,
	styles,
}
