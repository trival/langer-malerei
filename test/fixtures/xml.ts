export const infopage1 = `
<fmresultset xmlns="http://www.filemaker.com/xml/fmresultset" version="1.0">
<error code="0"/>
<product build="03/13/2013" name="FileMaker Web Publishing Engine" version="12.0.4.402"/>
<datasource database="langerEMV" date-format="MM/dd/yyyy" layout="p_Infoseiten" table="Kategorien_ZuInfo_Infoseiten" time-format="HH:mm:ss" timestamp-format="MM/dd/yyyy HH:mm:ss" total-count="17"/>
<metadata>
<field-definition auto-enter="yes" four-digit-year="no" global="no" max-repeat="1" name="id" not-empty="no" numeric-only="no" result="number" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="bild" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="titel_deu" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="titel_eng" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="titel_chi" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="calculation"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="kurzinfo_deu" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="kurzinfo_eng" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="kurzinfo_chi" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="calculation"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="Kategorien_ZuInfo_Infoseiten_freiHTML::deu" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="Kategorien_ZuInfo_Infoseiten_freiHTML::eng" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="Kategorien_ZuInfo_Infoseiten_freiHTML::chi" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="calculation"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="anmerkungen" not-empty="no" numeric-only="no" result="text" time-of-day="no" type="normal"/>
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="SortPrio" not-empty="no" numeric-only="no" result="number" time-of-day="no" type="normal"/>
<relatedset-definition table="Kategorien_ZuInfo">
<field-definition auto-enter="no" four-digit-year="no" global="no" max-repeat="1" name="Kategorien_ZuInfo::kategoroie_id" not-empty="no" numeric-only="no" result="number" time-of-day="no" type="normal"/>
</relatedset-definition>
</metadata>
<resultset count="17" fetch-size="1">
<record mod-id="28" record-id="1">
<field name="id">
<data>1</data>
</field>
<field name="bild">
<data>/Bilder300/Anfahrtskarte nah 04 ma_wPZ.png</data>
</field>
<field name="titel_deu">
<data>Anfahrtskarte</data>
</field>
<field name="titel_eng">
<data>Trip to us</data>
</field>
<field name="titel_chi">
<data>Trip to us</data>
</field>
<field name="kurzinfo_deu">
<data>Anfahrtsbeschreibung und Anfahrtsadressen</data>
</field>
<field name="kurzinfo_eng">
<data/>
</field>
<field name="kurzinfo_chi">
<data/>
</field>
<field name="Kategorien_ZuInfo_Infoseiten_freiHTML::deu">
<data/>
</field>
<field name="Kategorien_ZuInfo_Infoseiten_freiHTML::eng">
<data/>
</field>
<field name="Kategorien_ZuInfo_Infoseiten_freiHTML::chi">
<data/>
</field>
<field name="anmerkungen">
<data/>
</field>
<field name="SortPrio">
<data>88</data>
</field>
<relatedset count="2" table="Kategorien_ZuInfo">
<record mod-id="1" record-id="8">
<field name="Kategorien_ZuInfo::kategoroie_id">
<data>28</data>
</field>
</record>
<record mod-id="1" record-id="9">
<field name="Kategorien_ZuInfo::kategoroie_id">
<data>30</data>
</field>
</record>
</relatedset>
</record>
</resultset>
</fmresultset>
`

export const empty = `<?xml version ="1.0" encoding="UTF-8" standalone="no" ?><!DOCTYPE fmresultset PUBLIC "-//FMI//DTD fmresultset//EN" "http://moritz.filemakerhosting.de:80/fmi/xml/fmresultset.dtd"><fmresultset xmlns="http://www.filemaker.com/xml/fmresultset" version="1.0"><error code="401"></error><product build="03/13/2013" name="FileMaker Web Publishing Engine" version="12.0.4.402"></product><datasource database="" date-format="" layout="" table="" time-format="" timestamp-format="" total-count="0"></datasource><metadata></metadata><resultset count="0" fetch-size="0"></resultset></fmresultset>`
