import * as string from '../../src/utils/string'
import { expect } from '../test'

describe('utils -> string', () => {
	describe('clearHtmlData', () => {
		it('deletes \r', () => {
			expect(string.clearHtmlData('\r')).to.equal('')
		})
	})

	describe('mergeJson', () => {
		it('merges objects and jasonify them', () => {
			const obj1 = {
				foo: 'foo1',
				bar: 'bar1',
			}
			const obj2 = {
				bar: 'bar2',
				baz: 'baz2',
			}
			const obj3 = {
				foo: 'foo3',
				baz: 'baz3',
			}
			expect(string.mergeJson(obj1, obj2)).to.equal(
				JSON.stringify(Object.assign({}, obj1, obj2), null, '   '),
			)
			expect(string.mergeJson(obj1, obj2, obj3)).to.equal(
				JSON.stringify(Object.assign({}, obj1, obj2, obj3), null, '   '),
			)
		})

		it('stringifies Errors', () => {
			const e = JSON.parse(
				string.mergeJson(new TypeError('foo'), new Error('bar')),
			)
			expect(e.message).to.equal('bar')
			expect(e.stack).to.be.a('string')
		})
	})

	describe('objectifyError', () => {
		it('makes errors stringifiable', () => {
			const error = new TypeError('fufufu')

			const err = string.objectifyError(error),
				json = JSON.stringify(err),
				obj = JSON.parse(json)

			expect(error).to.not.equal(err)
			expect(obj.message).to.equal('fufufu')
			expect(obj.stack).to.be.a('string')
		})

		it('leaves others than errors undefined', () => {
			const obj = { fuu: 1, baa: 2 },
				e1 = string.objectifyError(null),
				e2 = string.objectifyError(obj),
				e3 = string.objectifyError()

			expect(e1).to.equal(null)
			expect(e2).to.equal(obj)
			expect(e3).to.be.undefined
		})
	})

	describe('splitNonEmpty', () => {
		it('splits a string with a char and trim', () => {
			expect(string.splitTrim('asdf ; qwer;		ou', ';')).to.deep.equal([
				'asdf',
				'qwer',
				'ou',
			])
		})

		it('returns an empty array if no valid result found', () => {
			expect(string.splitTrim('ou ', ';')).to.deep.equal(['ou'])
			expect(string.splitTrim('   ', ';')).to.deep.equal([])
			expect(string.splitTrim('   ', ';')).to.deep.equal([])
		})
	})
})
