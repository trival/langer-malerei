import { readdirSync, readFileSync } from 'fs'
import { resolve } from 'path'
import * as fmData from '../../src/utils/fm-data'
import * as xml from '../../src/utils/xml'
import * as fix from '../fixtures/xml'
import { expect } from '../test'

describe('utils -> data', () => {
	describe('getFmResults', () => {
		const testFilesDir = resolve(__dirname, '../fixtures/testXML')

		it('can read parsed filemaker xml resultsets', () => {
			const xmlData = readdirSync(testFilesDir)
				.filter(filename => /xml/.test(filename))
				.map(fileName => {
					return readFileSync(resolve(testFilesDir, fileName), 'utf8')
				})
				.map(xmlString => {
					return xml.parseXML(xmlString)
				})

			return Promise.all(
				xmlData.map(xmlPromise => {
					return xmlPromise.then(results => {
						const data = fmData.getFmResults(results)
						expect(data.currentSize).to.be.greaterThan(0)
						expect(data.totalSize).to.be.greaterThan(0)
						expect(data.results.length).to.be.greaterThan(0)
					})
				}),
			)
		})

		it('parses relatedsets into objects', () => {
			return xml.parseXML(fix.infopage1).then(xmlData => {
				const data = fmData.getFmResults(xmlData)
				const page = data.results[0]
				expect(data.results.length).equals(1)
				expect(data.currentSize).equals(1)
				expect(data.totalSize).equals(17)
				expect(page).deep.equals({
					id: '1',
					bild: '/Bilder300/Anfahrtskarte nah 04 ma_wPZ.png',
					titel_deu: 'Anfahrtskarte',
					titel_eng: 'Trip to us',
					titel_chi: 'Trip to us',
					kurzinfo_deu: 'Anfahrtsbeschreibung und Anfahrtsadressen',
					kurzinfo_eng: '',
					kurzinfo_chi: '',
					'Kategorien_ZuInfo_Infoseiten_freiHTML::deu': '',
					'Kategorien_ZuInfo_Infoseiten_freiHTML::eng': '',
					'Kategorien_ZuInfo_Infoseiten_freiHTML::chi': '',
					anmerkungen: '',
					SortPrio: '88',
					Kategorien_ZuInfo: [
						{ 'Kategorien_ZuInfo::kategoroie_id': '28' },
						{ 'Kategorien_ZuInfo::kategoroie_id': '30' },
					],
				})
			})
		})

		it('handles empty result gracefully', () => {
			return xml.parseXML(fix.empty).then(xmlData => {
				const emptyData = fmData.getFmResults(xmlData)
				expect(emptyData).to.deep.equal({
					currentSize: 0,
					totalSize: 0,
					results: [],
				})
			})
		})
	})

	describe('transform', () => {
		const transform = fmData.transform
		const data = {
			foo: 'fooValue',
			bar: 'barValue',
		}

		it('it returns an empty with empty transform spec', () => {
			expect(transform(data, {})).to.deep.equal({})
		})

		it('can allow and rename a given property', () => {
			const spec1: any = {
				foo: 'foo',
			}
			const spec2: any = {
				baz: 'bar',
			}

			expect(transform(data, spec1)).to.deep.equal({ foo: 'fooValue' })

			expect(transform(data, spec2)).to.deep.equal({ baz: 'barValue' })
		})

		it('can handle translations', () => {
			const spec: any = {
				name: {
					de: 'foo',
					en: {
						require: ['bar'],
						update: bar => bar + 'lala',
					},
				},
			}

			expect(transform(data, spec)).to.deep.equal({
				name: {
					de: 'fooValue',
					en: 'barValuelala',
				},
			})
		})

		it('can transform the values with functions', () => {
			const spec: any = {
				xfoo: {
					require: ['foo'],
					update: foo => 'x' + foo,
				},
				bubu: {
					require: ['foo', 'bar'],
					update: (foo, bar) => foo + '/' + bar,
				},
			}

			expect(transform(data, spec)).to.deep.equal({
				xfoo: 'xfooValue',
				bubu: 'fooValue/barValue',
			})
		})
	})

	describe('parseDataArray', () => {
		it('parses a data array from string', () => {
			const data1 = '1, 2, 3, 4,'
			const data2 = 'asdf df ;ssdf;   asdf ;	ff'

			expect(fmData.parseDataArray(data1, ',')).to.deep.equal([
				'1',
				'2',
				'3',
				'4',
			])

			expect(fmData.parseDataArray(data2, ';')).to.deep.equal([
				'asdf df',
				'ssdf',
				'asdf',
				'ff',
			])
		})
	})
})
