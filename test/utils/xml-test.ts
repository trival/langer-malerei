import { expect, mock } from '../test'
mock.stop('../../src/utils/xml')

import * as xml from '../../src/utils/xml'

describe('utils -> xml', () => {
	describe('parseXML', () => {
		it('parses XML', () => {
			return expect(
				xml.parseXML('<foo><bar>baz</bar></foo>'),
			).to.eventually.deep.equal({ foo: { bar: ['baz'] } })
		})
	})
})
