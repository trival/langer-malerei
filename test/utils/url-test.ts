import { assetPath } from '../../src/server/config'
import * as url from '../../src/utils/url'
import { expect } from '../test'

describe('utils -> url', () => {
	describe('assetUrl', () => {
		it('prepends the assetUrl', () => {
			expect(url.assetUrl('fufu')).to.equal(assetPath + '/fufu')
			expect(url.assetUrl('/fufu')).to.equal(assetPath + '/fufu')
			expect(url.assetUrl(' /fufu   ')).to.equal(assetPath + '/fufu')
			expect(url.assetUrl()).to.equal('')
			expect(url.assetUrl('  ')).to.equal('')
		})
	})

	describe('prepend', () => {
		it('prepends a prefix to a root url', () => {
			expect(url.prepend('fu', 'ba')).to.equal('ba/fu')
			expect(url.prepend('/fu', 'ba')).to.equal('ba/fu')
		})

		it('trims urls', () => {
			expect(url.prepend('  fu ', 'ba')).to.equal('ba/fu')
			expect(url.prepend('	/fu', '   ba ')).to.equal('ba/fu')
		})

		it('handles undefined urls', () => {
			expect(url.prepend()).to.equal('')
			expect(url.prepend('   ', '	')).to.equal('')
			expect(url.prepend('fuu')).to.equal('fuu')
			expect(url.prepend(undefined, 'kuku')).to.equal('')
		})
	})

	describe('urlize', () => {
		it('trims and replaces whitespaces', () => {
			expect(url.urlize(' asdf  a sdf  asdf ')).to.equal('asdf-a-sdf-asdf')
		})

		it('lowercases everything', () => {
			const s = 'sdfOUIdfjasUULL'
			expect(url.urlize(s)).to.equal(s.toLowerCase())
		})

		it('replaces various characters', () => {
			expect(url.urlize('ä')).to.equal('ae')
			expect(url.urlize('Ä ä')).to.equal('ae-ae')
			expect(url.urlize('ö')).to.equal('oe')
			expect(url.urlize('ü')).to.equal('ue')
			expect(url.urlize('ß')).to.equal('ss')
			expect(url.urlize('_k')).to.equal('-k')
			expect(url.urlize('__k')).to.equal('-k')
			expect(url.urlize('- _k')).to.equal('-k')
			expect(url.urlize('fu/ ba')).to.equal('fu-ba')
			expect(url.urlize('(k')).to.equal('k')
			expect(url.urlize(')k')).to.equal('k')
			expect(url.urlize('.k')).to.equal('-k')
			expect(url.urlize(',k')).to.equal('-k')
		})

		it('url nonstrings', () => {
			expect(url.urlize(21)).to.equal('21')
		})

		it('trims last -', () => {
			expect(url.urlize('fuu--')).to.equal('fuu')
		})
	})
})
