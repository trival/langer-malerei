import * as date from '../../src/utils/date'
import { expect } from '../test'

describe('utils -> date', () => {
	describe('parseMdy', () => {
		it('parses a date', () => {
			const d = date.parseMdy('10/30/2001')
			expect(d).to.be.instanceOf(Date)
			expect(d.toDateString()).to.equal('Tue Oct 30 2001')
		})
	})

	describe('formatDateDEShort', () => {
		it('returns date in a short german format', () => {
			const d = date.parseMdy('3/2/2001')
			const d2 = date.parseMdy('03/02/2001')
			expect(date.formatDateDEShort(d)).to.equal('02.03.2001')
			expect(date.formatDateDEShort(d2)).to.equal('02.03.2001')
		})

		it('parses dates from string', () => {
			const d = date.parseMdy('3/2/2001')
			expect(date.formatDateDEShort(d.toISOString())).to.equal('02.03.2001')
		})

		it('returns empty string if handed a none date', () => {
			expect((date.formatDateDEShort as any)(null)).to.equal('')
			expect((date.formatDateDEShort as any)()).to.equal('')
		})
	})
})
