import * as chai from 'chai'
import chaiPromise from 'chai-as-promised'
import mockRequire from 'mock-require'
import * as Sinon from 'sinon'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)
chai.use(chaiPromise)

export const expect = chai.expect
export const sinon = Sinon
export const mock = mockRequire

export const requestMock = sinon.stub()
requestMock.callsArgWith(1, null, null, 'remoteData')
mockRequire('request', requestMock)
