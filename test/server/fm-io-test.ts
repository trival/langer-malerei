import { expect, requestMock, sinon } from '../test'

import * as config from '../../src/server/config'
import * as io from '../../src/server/fm-io'
import * as fmData from '../../src/utils/fm-data'
import * as xml from '../../src/utils/xml'

const fm = config.filemaker

describe('fm-io', () => {
	const spec = {
		fmLayoutName: 'p_test',
		transformSpec: {
			id: 'foo',
		},
	}

	beforeEach(() => {
		requestMock.resetHistory()
		sinon
			.stub(xml, 'parseXML')
			.withArgs('remoteData')
			.returns(Promise.resolve('xmlData'))

		sinon
			.stub(fmData, 'getFmResults')
			.withArgs('xmlData')
			.returns({
				results: ['resultData1', 'resultData2'],
			} as any)

		sinon
			.stub(fmData, 'transform')
			.withArgs('resultData1', spec.transformSpec)
			.returns('data1' as any)
			.withArgs('resultData2', spec.transformSpec)
			.returns('data2' as any)
	})

	afterEach(() => {
		;(xml.parseXML as any).restore()
		;(fmData.getFmResults as any).restore()
		;(fmData.transform as any).restore()
	})

	describe('mocks', () => {
		it('are defined', () => {
			expect((io.libs.request as any).reset).to.be.a('function')
			return (io.libs.request as any)().then(a =>
				expect(a).deep.equals('remoteData'),
			)
		})
	})

	describe('getAllRemoteXMLData', () => {
		it('returns xml data from remote', () => {
			return io.getAllRemoteXMLData(spec).then(data => {
				const url = requestMock.args[0][0]
				expect(data).deep.equals({
					results: ['resultData1', 'resultData2'],
				})
				expect(url).to.match(/p_test/)
				expect(url.indexOf(fm.paramFindAll) >= 0).to.be.true
				expect(url.indexOf(fm.paramMax + '200') > 0).to.be.true
				expect(url.indexOf(fm.paramSkip + '0') >= 0).to.be.true
			})
		})

		it('can optionally maximize the amount of datasets', () => {
			return io.getAllRemoteXMLData(spec, 10, 20).then(data => {
				const url = requestMock.args[0][0]
				expect(data).deep.equals({
					results: ['resultData1', 'resultData2'],
				})
				expect(url.indexOf(fm.paramMax + '10') >= 0).to.be.true
				expect(url.indexOf(fm.paramSkip + '20') >= 0).to.be.true
			})
		})

		it('can optionally maximize the amount of datasets', () => {
			return io.getAllRemoteXMLData(spec, 10).then(data => {
				const url = requestMock.args[0][0]
				expect(data).deep.equals({
					results: ['resultData1', 'resultData2'],
				})
				expect(url.indexOf(fm.paramMax + '10') >= 0).to.be.true
			})
		})
	})

	describe('getSingleRemoteXMLData', () => {
		it('gets a single data set from fm', () => {
			return io.getSingleRemoteXMLData(spec, '12').then(data => {
				const url = requestMock.args[0][0]
				expect(data).deep.equals({
					results: ['resultData1', 'resultData2'],
				})
				expect(url).to.match(/p_test/)
				expect(url.indexOf(fm.paramFindAll) < 0).to.be.true
				expect(url.indexOf(fm.paramFindByQuery) >= 0).to.be.true
				expect(url.indexOf('&foo=12') >= 0).to.be.true
			})
		})
	})

	describe('transformFromXML', () => {
		it('transforms xml data into proper spec representation', () => {
			const data = io.transformFromFMResult(spec, {
				results: ['resultData1', 'resultData2'],
				currentSize: 2,
				totalSize: 0,
			})

			expect(data).to.deep.equal({
				entities: ['data1', 'data2'],
				raw: ['resultData1', 'resultData2'],
			})
		})
	})
})
