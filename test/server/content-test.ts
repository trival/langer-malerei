import * as article from '../../src/entities/article'
import * as exhibition from '../../src/entities/exhibition'
import * as workXexhibit from '../../src/entities/exhibitionXArticle'
import * as page from '../../src/entities/page'
import * as workXpage from '../../src/entities/pageXArticle'
import * as translations from '../../src/entities/translation'
import * as content from '../../src/server/content'
import * as io from '../../src/server/fm-io'
import { expect, sinon } from '../test'

const article1 = {
	id: '1',
	name: 'article1',
}

const article2 = {
	id: '2',
	name: 'article2',
}

const article3 = {
	id: '3',
	name: 'article3',
}

const page1 = {
	id: '1',
	name: 'page1',
}

const page2 = {
	id: '5',
	name: 'page2',
	articles: [],
}

const exhibition1 = {
	id: '1',
	name: 'exhibition1',
}

const exhibition2 = {
	id: '2',
	name: 'exhibition2',
}

const workXpage1 = {
	id: '1',
	workId: '2',
	pageId: '1',
}

const workXexhibit1 = {
	id: '1',
	workId: '1',
	exhibitionId: '1',
}

describe('content', () => {
	beforeEach(() => {
		const getAll = sinon.stub(io, 'getAllRemoteXMLData')
		getAll.withArgs(article.spec).returns(Promise.resolve('articles' as any))
		getAll.withArgs(page.spec).returns(Promise.resolve('pages' as any))
		getAll
			.withArgs(exhibition.spec)
			.returns(Promise.resolve('exhibitions' as any))
		getAll
			.withArgs(workXexhibit.spec)
			.returns(Promise.resolve('workXexhibit' as any))
		getAll.withArgs(workXpage.spec).returns(Promise.resolve('workXpage' as any))
		getAll
			.withArgs(translations.spec)
			.returns(Promise.resolve('translations' as any))

		const getSingle = sinon.stub(io, 'getSingleRemoteXMLData')
		getSingle.returns(Promise.resolve('single') as any)

		const getByQuery = sinon.stub(io, 'getRemoteXMLDataByQuery')
		getByQuery.returns(Promise.resolve('byQuery') as any)

		const collectAll = sinon.stub(io, 'collectAllData')
		collectAll
			.withArgs(article.spec)
			.returns(Promise.resolve('articles' as any))
		collectAll.withArgs(page.spec).returns(Promise.resolve('pages' as any))
		collectAll
			.withArgs(exhibition.spec)
			.returns(Promise.resolve('exhibitions' as any))
		collectAll
			.withArgs(workXexhibit.spec)
			.returns(Promise.resolve('workXexhibit' as any))
		collectAll
			.withArgs(workXpage.spec)
			.returns(Promise.resolve('workXpage' as any))
		collectAll
			.withArgs(translations.spec)
			.returns(Promise.resolve('translations' as any))

		const transform = sinon.stub(io, 'transformFromFMResult')
		transform
			.withArgs(article.spec, 'articles' as any)
			.onFirstCall()
			.returns([article1, article2])
			.onSecondCall()
			.returns([article2, article3])
		transform
			.withArgs(article.spec, 'byQuery' as any)
			.onFirstCall()
			.returns([article1, article2])
			.onSecondCall()
			.returns([article2, article3])
		transform.withArgs(page.spec, 'pages' as any).returns([page1, page2])
		transform
			.withArgs(exhibition.spec, 'exhibitions' as any)
			.returns([exhibition1, exhibition2])
		transform
			.withArgs(workXexhibit.spec, 'workXexhibit' as any)
			.returns([workXexhibit1])
		transform.withArgs(workXpage.spec, 'workXpage' as any).returns([workXpage1])
		transform.withArgs(translations.spec, 'translations' as any).returns([])

		transform.withArgs(page.spec, 'single' as any).returns({
			entities: [page2],
		} as any)

		content.reset()
		;(exhibition1 as any).works = undefined
		;(exhibition2 as any).works = undefined
		;(article1 as any).exhibitions = undefined
		;(article2 as any).exhibitions = undefined
		;(article3 as any).exhibitions = undefined
	})

	afterEach(() => {
		;(io.getAllRemoteXMLData as any).restore()
		;(io.getSingleRemoteXMLData as any).restore()
		;(io.transformFromFMResult as any).restore()
		;(io.collectAllData as any).restore()
	})

	describe('getEntities', () => {
		it('can get entities without reload', async () => {
			const entities = await content.getEntities(true)
			expect(entities).to.deep.equal({
				articles: {},
				pages: {},
				exhibitions: {},
				translations: {},
			})
			expect(io.collectAllData).to.not.be.called
			expect(io.transformFromFMResult).to.not.be.called
		})

		it('reloads entities on first call', async () => {
			const entities = await content.getEntities()
			expect(entities).to.deep.equal({
				articles: {
					1: {
						...article1,
						exhibitions: ['1'],
					},
					2: article2,
				},
				pages: {
					1: page1,
					5: page2,
				},
				exhibitions: {
					1: {
						...exhibition1,
						works: ['1'],
					},
					2: exhibition2,
				},
				translations: {},
			})
			expect(io.collectAllData).to.be.calledWith(article.spec)
			expect(io.transformFromFMResult).to.be.calledWith(
				article.spec,
				'articles',
			)
		})

		it('doesnt reload when entities are not empty', async () => {
			const entities = await content.getEntities(true)
			;(entities.pages[1] as any) = page1

			const newEntities = await content.getEntities()
			expect(newEntities).to.deep.equal({
				articles: {},
				pages: { 1: page1 },
				exhibitions: {},
				translations: {},
			})

			expect(io.collectAllData).to.not.be.called
			expect(io.transformFromFMResult).to.not.be.called
		})
	})

	describe('reloadAll', () => {
		it('fetches all entities', async () => {
			const entities = await content.reloadAll()
			expect(entities).to.deep.equal({
				articles: {
					1: {
						...article1,
						exhibitions: ['1'],
					},
					2: article2,
				},
				pages: {
					1: page1,
					5: page2,
				},
				exhibitions: {
					1: {
						...exhibition1,
						works: ['1'],
					},
					2: exhibition2,
				},
				translations: {},
			})
		})

		it('fetches replaces entities with new entities on success', async () => {
			await content.reloadAll()
			await content.reloadAll()

			const entities = await content.getEntities()

			expect(entities).to.deep.equal({
				articles: {
					2: article2,
					3: article3,
				},
				pages: {
					1: page1,
					5: page2,
				},
				exhibitions: {
					1: {
						...exhibition1,
						works: ['1'],
					},
					2: exhibition2,
				},
				translations: {},
			})
		})
	})

	describe('reset', () => {
		it('resets the state of the content', async () => {
			const entities = await content.getEntities()
			expect(entities).to.deep.equal({
				articles: {
					1: {
						...article1,
						exhibitions: ['1'],
					},
					2: article2,
				},
				pages: {
					1: page1,
					5: page2,
				},
				exhibitions: {
					1: {
						...exhibition1,
						works: ['1'],
					},
					2: exhibition2,
				},
				translations: {},
			})

			content.reset()

			const newEntities = await content.getEntities(true)

			expect(newEntities).to.deep.equal({
				articles: {},
				pages: {},
				exhibitions: {},
				translations: {},
			})
		})
	})
})
