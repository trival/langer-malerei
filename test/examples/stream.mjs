import { Readable } from 'stream'

const stream = new Readable()

stream.pipe(process.stdout)

stream.push('1')
stream.push('2')
stream.push(null)
stream.on('error', e => console.error(e))
