/*
TS_NODE_COMPILER_OPTIONS='{"module":"commonjs"}' node -r ts-node/register/transpile-only -r dotenv/config test/examples/deleteSmallBucketElements.ts
*/

import { createS3Service, awsS3RootPath } from '../../src/server/s3service'

async function main() {
	const s3 = createS3Service()

	async function collectAll(results: any[] = [], continuationToken = '') {
		const result = await s3.getAll(continuationToken)
		results = results.concat(result.Contents)
		console.log({ ...result, Contents: undefined })
		if (result.NextMarker) {
			results = await collectAll(results, result.NextMarker)
		}
		return results
	}

	const items = await collectAll()

	console.log('total items', items.length)

	const small = items.filter((i) => i.Size < 8000)
	console.log('small items', small.length)

	for (const item of small) {
		const key = (item.Key as string).replace(awsS3RootPath + '/', '')
		console.log('deleting', key)
		const result = await s3.remove(key)
		console.log(result)
	}
}

main()
