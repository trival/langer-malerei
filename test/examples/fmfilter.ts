import * as article from '../../src/entities/article'
// import * as page from '../../src/entities/page'
import { getRemoteXMLDataByQuery } from '../../src/server/fm-io'
import moment from 'moment'

async function main() {
	const date = moment().subtract(2, 'day').format('MM/DD/YYYY')
	console.log(date)

	const results = await getRemoteXMLDataByQuery(
		article.spec,
		`bearbeitet=${date}&bearbeitet.op=gt`,
	)
	console.log(results)
}

main()

/*
TS_NODE_COMPILER_OPTIONS='{"module":"commonjs"}' node -r ts-node/register/transpile-only -r dotenv/config test/examples/fmfilter.ts
*/
