import { promisify } from 'util'
import * as xml from 'xml2js'

export function parseXML<T = any>(xmlString: string) {
	return promisify(xml.parseString)(xmlString) as Promise<T>
}
