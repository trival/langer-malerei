import slugify from 'slugify'
import { assetPath } from '../server/config'
import { Entities } from '../server/content'

export function urlize(s) {
	const replaceMap = [
		[/(\s|_|-|\/|\.|,)+/g, '-'],
		[/(\(|\))+/g, ''],
		[/ä/g, 'ae'],
		[/ö/g, 'oe'],
		[/ü/g, 'ue'],
		[/ß/g, 'ss'],
	]

	s = ('' + s).trim().toLowerCase()

	for (const i in replaceMap) {
		s = s.replace.apply(s, replaceMap[i])
	}

	if (s[s.length - 1] === '-') {
		s = s.substring(0, s.length - 1)
	}

	return s
}

export function prepend(url?: string, prefix?: string): string {
	url = url && url.trim()
	prefix = prefix && prefix.trim()

	if (!url) {
		return ''
	} else if (!prefix) {
		return url
	}

	if (url[0] === '/') {
		return prefix + url
	} else {
		return prefix + '/' + url
	}
}

export function assetUrl(url?: string) {
	return prepend(url, assetPath)
}

export function isImage(url) {
	return /\.(jpg|jpeg|png|gif)/.test(url.toLowerCase())
}

export interface URLOptions {
	entities: Entities
	lang: string
	path?: string
	id?: string
	page?: string
	exhibition?: string
}

const slugOpts = { lower: true }

function getPageUrl(id: string, lang: string, entities: Entities) {
	const page = entities.pages[id as string]
	const name = (page && (page.name[lang] || page.title[lang])) || 'untitled'
	return '/page/' + slugify(name, slugOpts) + '/' + id
}

function getExhibitionUrl(id: string, entities: Entities) {
	const exhibition = entities.exhibitions[id as string]
	const name =
		(exhibition && exhibition.name + '-' + exhibition.year) || 'untitled'
	return '/exhibition/' + slugify(name, slugOpts) + '/' + id
}

function getArtworkUrl(id: string, lang: string, entities: Entities) {
	const work = entities.articles[id as string]
	const name =
		(work &&
			(work.name[lang] || 'untitled') +
				'-' +
				new Date(work.date as string).getFullYear()) ||
		'untitled'
	return '/artwork/' + slugify(name, slugOpts) + '/' + id
}

export function toPath({
	lang,
	path,
	id,
	entities,
	page,
	exhibition,
}: URLOptions) {
	let url = '/' + lang
	if (path === 'page') {
		url += getPageUrl(id as string, lang, entities)
	}
	if (path === 'exhibition') {
		url += getExhibitionUrl(id as string, entities)
	}
	if (path === 'artwork') {
		// if (page) {
		// 	url += getPageUrl(page, lang, entities)
		// } else if (exhibition) {
		// 	url += getExhibitionUrl(exhibition, entities)
		// }
		url += getArtworkUrl(id as string, lang, entities)
		if (page) {
			url += '?page=' + page
		} else if (exhibition) {
			url += '?exhibition=' + exhibition
		}
	}
	return url
}
