export function createRandomGenerator(s) {
	let mw = 987654321 + s
	let mz = 123456789 - s
	const mask = 0xffffffff

	return () => {
		mz = (36969 * (mz & 65535) + (mz >> 16)) & mask
		mw = (18000 * (mw & 65535) + (mw >> 16)) & mask

		let result = ((mz << 16) + mw) & mask
		result /= 4294967296

		return result + 0.5
	}
}
