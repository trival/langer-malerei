import { I18nString } from '../entities/entity-types'
import { Page } from '../entities/page'
import { Language } from '../server/config'

export function clearHtmlData(s) {
	return s && s.replace(/\r/g, '')
}

export function splitTrim(str: string, separator: string) {
	return str
		.split(separator)
		.map(y => y.trim())
		.filter(z => z)
}

export function mergeJson(obj1, obj2?, obj3?) {
	return JSON.stringify(
		Object.assign(
			{},
			objectifyError(obj1),
			objectifyError(obj2),
			objectifyError(obj3),
		),
		null,
		'   ',
	)
}

export function objectifyError(e?) {
	if (e && typeof e === 'object' && e instanceof Error) {
		const props = Object.getOwnPropertyNames(e)
		const obj = {}
		for (const prop of props) {
			obj[prop] = e[prop]
		}
		return obj
	}
	return e
}

export function $t(t: I18nString | string, lang: string) {
	return typeof t === 'string' ? t : t[lang] || t[Language.DE]
}

export function getPageTitle(page: Page, language: Language) {
	const name = page.name[language]
	const title = page.title[language]

	let text = name || title
	if (
		name &&
		title &&
		name.trim().toLowerCase() !== title.trim().toLowerCase()
	) {
		text = name + ' - ' + title
	}
	return text
}
