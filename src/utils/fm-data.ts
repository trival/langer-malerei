import * as assert from 'assert'
import { Entity, TransformSpec, TransformValue } from '../entities/entity-types'
import { Language } from '../server/config'

export interface FMResults {
	currentSize: number
	totalSize: number
	results: any[]
}
/**
 * Extracts the data from a parsed filemaker xml resultset.
 * @return array the results as an array
 */
export function getFmResults(xmlData): FMResults {
	const resultset = xmlData.fmresultset.resultset[0]
	const results: FMResults = {
		// tslint:disable-next-line:radix
		currentSize: parseInt(resultset.$['fetch-size']),
		// tslint:disable-next-line:radix
		totalSize: parseInt(resultset.$.count),
		results: [],
	}

	if (!resultset.record) {
		return results
	}

	results.results = resultset.record.map(record => {
		const newRec = {}
		record.field.forEach(field => {
			newRec[field.$.name] = field.data[0]
		})
		if (record.relatedset) {
			record.relatedset.forEach(set => {
				if (set.record) {
					newRec[set.$.table] = set.record.map(rec => {
						const setRec = {}
						rec.field.forEach(setField => {
							setRec[setField.$.name] = setField.data[0]
						})
						return setRec
					})
				}
			})
		}
		return newRec
	})

	assert.equal(
		results.currentSize,
		results.results.length,
		'got not as many results as indicated by the fmresultset',
	)

	return results
}

function getTransformValue(data: any, specValue: TransformValue) {
	if (typeof specValue === 'string') {
		const value = data[specValue]
		return typeof value === 'string' ? value.trim() : value
	} else {
		const args = specValue.require.map(key => data[key])
		return specValue.update.apply(null, args)
	}
}

/**
 * Transforms a given data object according to the given spec.
 */
export function transform<T extends Entity>(
	data: any,
	spec: TransformSpec<T>,
): T {
	const result = {} as T

	for (const key in spec) {
		const specValue = spec[key]
		let value

		if (specValue[Language.DE] && specValue[Language.EN]) {
			value = {
				[Language.DE]: getTransformValue(data, specValue[Language.DE]),
				[Language.EN]: getTransformValue(data, specValue[Language.EN]),
			}
		} else {
			value = getTransformValue(data, specValue as TransformValue)
		}

		result[key] = value
	}
	return result
}

export function parseDataArray(data: string, separator: string) {
	const arr = data ? data.split(separator).map(id => id.trim()) : []

	if (arr[arr.length - 1] === '') {
		arr.pop()
	}

	return arr
}
