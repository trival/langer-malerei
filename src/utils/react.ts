export const preventAndStop = e => {
	e.stopPropagation && e.stopPropagation()
	e.preventDefault && e.preventDefault()
	e.stopImmediatePropagation && e.stopImmediatePropagation()
	return false
}
