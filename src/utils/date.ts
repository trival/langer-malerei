import moment from 'moment'

const monthNamesDE = [
	'Januar',
	'Februar',
	'März',
	'April',
	'Mai',
	'Juni',
	'Juli',
	'August',
	'September',
	'Oktober',
	'November',
	'Dezember',
]

function pad(num) {
	return ('0' + num).slice(-2)
}

export function parseMdy(str: string) {
	const parts = str.split('/')
	return new Date(
		// tslint:disable-next-line:radix
		parseInt(parts[2]), // year
		// tslint:disable-next-line:radix
		parseInt(parts[0]) - 1, // month
		// tslint:disable-next-line:radix
		parseInt(parts[1]), // day
	)
}

export function parseDate(str: string) {
	return moment(str.trim(), 'MM/DD/YYYY').toDate()
}

export function parseDateTime(str: string) {
	return moment(str.trim(), 'MM/DD/YYYY HH:mm:ss').toDate()
}

export function formatDateDEShort(date?: Date | string): string {
	if (typeof date === 'string') {
		date = new Date(date)
	}

	if (!date || !(date instanceof Date)) {
		return ''
	}

	const day = pad(date.getDate())
	const month = pad(date.getMonth() + 1)
	const year = date.getFullYear()

	return day + '.' + month + '.' + year
}

export function formatDateDE(date: Date | string): string {
	if (typeof date === 'string') {
		date = new Date(date)
	}

	const day = pad(date.getDate())
	const monthIndex = date.getMonth()
	const year = date.getFullYear()

	return day + '. ' + monthNamesDE[monthIndex] + ' ' + year
}

export function formatDateTimeDE(date: Date | string): string {
	if (typeof date === 'string') {
		date = new Date(date)
	}

	const dateStr = formatDateDE(date)
	const hours = pad(date.getHours())
	const minutes = pad(date.getMinutes())

	return dateStr + ' - ' + hours + ':' + minutes + ' Uhr'
}
