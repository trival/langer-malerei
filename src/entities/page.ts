import { splitTrim } from '../utils/string'
import { assetUrl } from '../utils/url'
import { Entity, I18nString, Spec } from './entity-types'
import { parseDateTime } from '../utils/date'
import { filemaker } from '../server/config'

/* =======================================
	Page spec
	------------

	Currently following properties come from filemaker layout Seiten:

	id
	pid
	inMenueZeigen
	sort
	Datum
	typid
	freitext_de
	freitext_en
	Menuname_de
	Menuname_en
	Titel_de
	Titel_en
	bild
	listChilds
	listArt
	bild2
	bild3 ... bild9
	bearbeitet

============================ */

export enum FixedPageIds {
	StartPage = '1',
	Literature = '2',
	Exhibitions = '3',
	Editions = '4',
	Gallery = '5',
	Videos = '6',
	Statements = '8',
	News = '9',
	Cartoon = '10',
	Imprint = '11',
	Privacy = '0',
}

export enum PageType {
	Standard = '1',
	Gallery = '2',
	Exhibitions = '3',
	Editions = '4',
	StartPage = '5',
	Statements = '6',
	Events = '7',
	Videos = '8',
	News = '9',
	Cartoon = '10',
}

export interface Page extends Entity {
	type: PageType
	name: I18nString
	title: I18nString
	text: I18nString
	parentId: string
	imgs: string[]
	showInMenu: boolean
	articles: string[]
	children: string[]
	updated: Date | string
}

export const spec: Spec<Page> = {
	fmLayoutName: 'Seiten',

	transformSpec: {
		id: 'id',
		parentId: 'pid',
		type: 'typid',

		name: {
			de: 'Menuname_de',
			en: 'Menuname_en',
		},

		title: {
			de: 'Titel_de',
			en: 'Titel_en',
		},

		text: {
			de: 'freitext_de',
			en: 'freitext_en',
		},

		imgs: {
			require: [
				'bearbeitet',
				'bild',
				'bild2',
				'bild3',
				'bild4',
				'bild5',
				'bild6',
				'bild7',
				'bild8',
				'bild9',
			],
			update: (date, ...args) => {
				const timestamp = parseDateTime(date)?.getTime()
				return args
					.filter((v) => v.trim())
					.map(assetUrl)
					.map((url) =>
						url.replace(filemaker.imgURL, `${filemaker.imgURL}${timestamp}-`),
					)
			},
		},

		showInMenu: {
			require: ['inMenueZeigen'],
			update: (x) => !!x.trim(),
		},

		articles: {
			require: ['listArt'],
			update: (x) => splitTrim(x, ';'),
		},

		children: {
			require: ['listChilds'],
			update: (x) => splitTrim(x, ';'),
		},

		updated: {
			require: ['bearbeitet'],
			update: parseDateTime,
		},
	},
}
