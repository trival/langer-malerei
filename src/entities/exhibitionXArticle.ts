import { Entity, Spec } from './entity-types'

/* =======================================
	ArtXExhibit spec
	------------

	Currently following properties come from filemaker layout ArtikelXAusstellungen:

	ArtikelID
	AusstellungID

============================ */

export interface WorkXExhibit extends Entity {
	workId: string
	exhibitionId: string
}

export const spec: Spec<WorkXExhibit> = {
	fmLayoutName: 'ArtikelXAusstellungen',

	transformSpec: {
		id: '',
		exhibitionId: 'AusstellungID',
		workId: 'ArtikelID',
	},
}
