import { Entity, Spec } from './entity-types'

/* =======================================
	Translation spec
	------------

	Currently following properties come from filemaker layout:

	id
	de
	en

============================ */

export interface Translation extends Entity {
	de: string
	en: string
}

export interface Translations {
	[id: string]: Translation
}

export enum TranslationIds {
	untitled = 23,
	websiteTitle = 24,
	exhibitions = 25,
	place = 26,
	gallery = 27,
	duration = 28,
	downloadDocument = 29,
	back = 30,
	edition = 31,
	paper = 22,
	price = 32,
}

export const spec: Spec<Translation> = {
	fmLayoutName: 'Tech_LangBricks',

	transformSpec: {
		id: 'id',
		de: 'de',
		en: 'en',
	},
}
