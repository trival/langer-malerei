import { Language } from '../server/config'

export interface Entity {
	id: string
}

export interface Collection<T extends Entity> {
	[id: string]: T
}

export type I18nString = { [K in Language]: string }

export type TransformValue =
	| string
	| {
			require: string[]
			update: (...args: any[]) => any
	  }

export type Translation = { [K in Language]: TransformValue }

export type TransformSpec<T extends Entity> = {
	[K in keyof T]: TransformValue | Translation
}

export interface Spec<T extends Entity> {
	transformSpec: TransformSpec<T>
	fmLayoutName: string
}
