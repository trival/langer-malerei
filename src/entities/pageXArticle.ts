import { Entity, Spec } from './entity-types'

/* =======================================
	ArtXExhibit spec
	------------

	Currently following properties come from filemaker layout SeitenXArt:

	id
	Seitenid
	Artid

============================ */

export interface WorkXPage extends Entity {
	workId: string
	pageId: string
}

export const spec: Spec<WorkXPage> = {
	fmLayoutName: 'SeitenXArt',

	transformSpec: {
		id: 'id',
		pageId: 'Seitenid',
		workId: 'Artid',
	},
}
