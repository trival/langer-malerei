import { parseDate, parseDateTime } from '../utils/date'
import { assetUrl } from '../utils/url'
import { Entity, I18nString, Spec } from './entity-types'
import { filemaker } from '../server/config'

/* =======================================
	Article spec
	------------

	Currently following properties come from filemaker layout webart:

		id

		Bildname_deu
		Bildname_eng

		jpg
		jpg_preview
		jpg_preview400

		jpg_dunkelheit
		jpg_schwarzlicht

		Datum
		erstellt
		bearbeitet

		Rahmen
		Genre
		Preisoption
		Auflage
		Malgrund::LangBrickID
		Technik::LangBrickID
		Papierhersteller
		Grammatur
		PreisÖffentlich
		VK

		bildkante1mm
		bildkante2mm
		rahmenkante1mm
		rahmenkante2mm

		Beschreibungstext_de
		Beschreibungstext_en

		attDeskription
		attFile
		attKeywords
		attTitel
		attZugeordnText

============================ */

export interface Article extends Entity {
	name: I18nString
	description: I18nString
	imgs: string[]
	imgPreview: string
	imgAlias: string
	date: Date | string
	created: Date | string
	updated: Date | string
	technique: string
	ground: string
	edition: string
	widthInner: string
	widthOuter: string
	heightInner: string
	heightOuter: string
	exhibitions?: string[]
	paper: string
	paperWeight: string
	publicPrice: boolean
	price: string
	altText: I18nString
	metaDescription: I18nString
	metaFile: I18nString
	metaKeywords: I18nString
	metaTitle: I18nString
}

export const spec: Spec<Article> = {
	fmLayoutName: 'webart',

	transformSpec: {
		id: 'id',

		date: {
			require: ['Datum'],
			update: parseDate,
		},

		created: {
			require: ['erstellt'],
			update: parseDateTime,
		},

		updated: {
			require: ['bearbeitet'],
			update: parseDateTime,
		},

		name: {
			de: 'Bildname_deu',
			en: 'Bildname_eng',
		},

		description: {
			de: 'Beschreibungstext_de',
			en: 'Beschreibungstext_en',
		},

		imgs: {
			require: ['bearbeitet', 'attFile', 'jpg_schwarzlicht', 'jpg_dunkelheit'],
			update: (date, ...args) => {
				const timestamp = parseDateTime(date)?.getTime()
				return args
					.filter((v) => v.trim())
					.map(assetUrl)
					.map((url) =>
						url.replace(filemaker.imgURL, `${filemaker.imgURL}${timestamp}-`),
					)
			},
		},

		imgAlias: {
			require: ['bearbeitet', 'jpg'],
			update: (date, img) => {
				const timestamp = parseDateTime(date)?.getTime()
				return img
					.trim()
					.replace(filemaker.imgURL, `${filemaker.imgURL}${timestamp}-`)
			},
		},

		imgPreview: {
			require: ['bearbeitet', 'jpg_preview400'],
			update: (date, img) => {
				const timestamp = parseDateTime(date)?.getTime()
				return assetUrl(img.trim()).replace(
					filemaker.imgURL,
					`${filemaker.imgURL}${timestamp}-`,
				)
			},
		},

		technique: 'Technik::LangBrickID',
		ground: 'Malgrund::LangBrickID',
		edition: {
			require: ['Auflage'],
			// tslint:disable-next-line:radix
			update: (v) => (parseInt(v) > 1 ? v : ''),
		},

		widthInner: 'bildkante1mm',
		heightInner: 'bildkante2mm',
		widthOuter: 'rahmenkante1mm',
		heightOuter: 'rahmenkante2mm',

		paper: 'Papierhersteller',
		paperWeight: 'Grammatur',

		publicPrice: 'PreisÖffentlich',
		price: 'VK',

		altText: {
			de: 'attZugeordnText',
			en: 'attZugeordnText_en',
		},
		metaTitle: {
			de: 'attTitel',
			en: 'attTitel_en',
		},
		metaDescription: {
			de: 'attDeskription',
			en: 'attDeskription_en',
		},
		metaKeywords: {
			de: 'attKeywords',
			en: 'attKeywords_en',
		},
		metaFile: {
			de: 'attFile',
			en: 'attFile_en',
		},
	},
}
