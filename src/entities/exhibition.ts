import { parseMdy } from '../utils/date'
import { Entity, Spec } from './entity-types'

/* =======================================
	Exhibition spec
	------------

	Currently following properties come from filemaker layout webausstellungen:

	id
	Name
	Jahr
	Galerie
	Ort
	Zusammenfassung
	GalerieVerschweigen
	imWeb
	DatumBeginn
	DatumEnde
	TerminZirka



============================ */

export interface Exhibition extends Entity {
	name: string
	year: string
	gallery: string
	place: string
	description: string
	hideGallery: boolean
	public: boolean
	startDate: Date | string
	endDate: Date | string
	dateEstimate: string
	works?: string[]
}

export const spec: Spec<Exhibition> = {
	fmLayoutName: 'webausstellungen',

	transformSpec: {
		id: 'id',
		name: 'Name',
		year: 'Jahr',
		gallery: 'Galerie',
		place: 'Ort',
		description: 'Zusammenfassung',
		hideGallery: {
			require: ['GalerieVerschweigen'],
			update: x => !!x.trim(),
		},
		public: {
			require: ['imWeb'],
			update: x => !!x.trim(),
		},
		startDate: {
			require: ['DatumBeginn'],
			update: parseMdy,
		},
		endDate: {
			require: ['DatumEnde'],
			update: parseMdy,
		},
		dateEstimate: 'TerminZirka',
	},
}
