import * as React from 'react'
import { FixedPageIds } from '../entities/page'
import { $t } from '../utils/string'
import { Consumer } from './context'
import Link from './Link'

const MainFooter: React.StatelessComponent = () => {
	return (
		<Consumer>
			{ctx => {
				const { language: lang, pages } = ctx
				const imprint = pages[FixedPageIds.Imprint]
				const privacy = pages[FixedPageIds.Privacy]

				return (
					<footer className="main-footer">
						<Link path="page" id={FixedPageIds.Imprint}>
							<a>{$t(imprint.name, lang)}</a>
						</Link>
						<Link path="page" id={FixedPageIds.Privacy}>
							<a>{$t(privacy.name, lang)}</a>
						</Link>
						<p className="footer-text-line">
							© {new Date().getFullYear()} Gunter Langer
						</p>
					</footer>
				)
			}}
		</Consumer>
	)
}

export default MainFooter
