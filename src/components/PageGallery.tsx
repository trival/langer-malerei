import * as React from 'react'
import { Page } from '../entities/page'
import { $t } from '../utils/string'
import { Consumer } from './context'
import Link from './Link'
import PageStandard from './PageStandard'
import Thumb from './Thumbnail'

interface Props {
	page: Page
	workIds: string[]
}

const PageGallery: React.StatelessComponent<Props> = ({ page, workIds }) => (
	<Consumer>
		{ctx => {
			const lang = ctx.language
			const articles = workIds.map(id => ctx.articles[id]).filter(x => x)

			return (
				<PageStandard page={page} language={lang}>
					{articles.map(a => (
						<Link path="artwork" query={{ page: page.id }} id={a.id} key={a.id}>
							<a className="preview-link work-preview">
								<Thumb src={a.imgPreview} alt={$t(a.metaTitle, lang)} />
							</a>
						</Link>
					))}
				</PageStandard>
			)
		}}
	</Consumer>
)

export default PageGallery
