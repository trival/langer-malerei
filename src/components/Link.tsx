import Link from 'next/link'
import React from 'react'
import { toPath } from '../utils/url'
import { Consumer, Context } from './context'

interface Props {
	lang?: string
	id?: string
	path?: string
	query?: any
	children: any
}

const MyLink: React.StatelessComponent<Props> = ({
	children,
	path,
	id,
	query,
	lang,
}) => (
	<Consumer>
		{(ctx: Context) => {
			lang = lang || ctx.language
			const url = toPath({
				entities: ctx,
				lang,
				id,
				path,
				...query,
			})
			return (
				<Link
					as={url}
					href={{ pathname: '/' + (path || ''), query: { lang, ...query, id } }}
				>
					{children}
				</Link>
			)
		}}
	</Consumer>
)

export default MyLink
