import * as markdownIt from 'markdown-it'
import React from 'react'

const md = markdownIt({
	breaks: true,
	html: true,
	linkify: true,
	typographer: true,
})

interface Props {
	markdown: string
}

const MarkdownContent: React.StatelessComponent<Props> = ({ markdown }) => {
	markdown = (markdown && markdown.replace('] (', '](')) || ''
	const html = md.render(markdown)
	return (
		<article
			className="markdown-content"
			dangerouslySetInnerHTML={{ __html: html }}
		/>
	)
}

export default MarkdownContent
