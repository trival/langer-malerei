import * as React from 'react'
import { Close } from './icons/Close'
import { ZoomIn } from './icons/ZoomIn'
import { ZoomOut } from './icons/ZoomOut'

interface Props {
	imgUrl: string
	onClose: () => void
}

interface State {
	big: boolean
}

class ImgDetails extends React.Component<Props, State> {
	constructor(props) {
		super(props)
		this.state = {
			big: false,
		}
	}

	toggleZoom = () => {
		this.setState(s => ({ ...s, big: !s.big }))
	}

	render() {
		const url = this.props.imgUrl
		return (
			<article className="image-details">
				<header>
					<nav>
						<button className="resize" onClick={this.toggleZoom}>
							{this.state.big ? <ZoomOut /> : <ZoomIn />}
						</button>
						<button className="close" onClick={this.props.onClose}>
							<Close />
						</button>
					</nav>
				</header>

				<section className="image">
					<img className={this.state.big ? 'big' : ''} src={url} />
				</section>

				<style>{`body, html {
						overflow: hidden;
					}`}</style>
			</article>
		)
	}
}

export default ImgDetails
