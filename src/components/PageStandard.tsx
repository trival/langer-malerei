import * as React from 'react'
import { Page } from '../entities/page'
import { Language } from '../server/config'
import { $t } from '../utils/string'
import MarkDown from './MarkdownContent'

interface Props {
	page: Page
	language: Language
}

const PageStandard: React.StatelessComponent<Props> = ({
	page,
	language: lang,
	children,
}) => (
	<article>
		{page.title && $t(page.title, lang) && (
			<header>
				<h2>{$t(page.title, lang)}</h2>
			</header>
		)}

		{children && (children as any).length > 0 && (
			<section className="collection">{children}</section>
		)}

		<section className="text-content">
			<MarkDown markdown={$t(page.text, lang)} />
		</section>
	</article>
)

export default PageStandard
