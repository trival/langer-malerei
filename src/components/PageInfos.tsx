import * as React from 'react'
import { Page } from '../entities/page'
import { Language } from '../server/config'
import { Entities } from '../server/content'
import Info from './InfoBlock'
import PageStandard from './PageStandard'

interface Props {
	page: Page
	entities: Entities
	language: Language
}

class PageInfos extends React.Component<Props, {}> {
	render() {
		const lang = this.props.language
		const page = this.props.page
		const pages = this.props.entities.pages

		const currentPages = page.children.map(id => pages[id])

		return (
			<PageStandard page={page} language={lang}>
				{currentPages.map(p => (
					<Info page={p} key={'ip' + p.id} />
				))}
			</PageStandard>
		)
	}
}

export default PageInfos
