import React from 'react'

interface Props {
	src: string
	alt?: string
}

let observer: IntersectionObserver | undefined

if (typeof window !== 'undefined') {
	// workaround variable w to avoid minification bug on safari
	const w = window as any
	if (typeof w.IntersectionObserver !== 'undefined') {
		const I = w.IntersectionObserver as typeof IntersectionObserver
		observer = new I((entries, observer) => {
			entries.forEach(entry => {
				if (entry.intersectionRatio > 0 || entry.isIntersecting) {
					observer.unobserve(entry.target)
					const img = entry.target.querySelector('img')
					if (img) {
						img.src = img.dataset.src as string
					}
				}
			})
		})
	}
}

export class Thumbnail extends React.Component<Props> {
	thumbRef = React.createRef<HTMLElement>()

	componentDidMount() {
		const thumb = this.thumbRef.current
		if (observer && thumb) {
			observer.observe(thumb)
		} else if (thumb) {
			const img = thumb.querySelector('img')
			if (img) {
				img.src = img.dataset.src as string
			}
		}
	}

	componentWillUnmount() {
		if (observer && this.thumbRef.current) {
			observer.unobserve(this.thumbRef.current)
		}
	}

	render() {
		return (
			<figure ref={this.thumbRef} className="thumbnail">
				<div className="image">
					<img
						src="/static/imgs/empty.png"
						data-src={this.props.src}
						alt={this.props.alt}
					/>
				</div>
			</figure>
		)
	}
}

export default Thumbnail
