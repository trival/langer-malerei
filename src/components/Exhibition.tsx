import Router from 'next/router'
import * as React from 'react'
import { Exhibition } from '../entities/exhibition'
import { formatDateDEShort } from '../utils/date'
import { toPath } from '../utils/url'
import { Consumer } from './context'
import MarkDown from './MarkdownContent'

interface Props {
	exhibition: Exhibition
	language: string
	short?: boolean
}

const ExhibitionItem: React.StatelessComponent<Props> = ({
	exhibition: e,
	short,
	language,
}) => {
	const isLink = e.works && e.works.length > 0

	const onClick = entities => () =>
		isLink &&
		Router.push(
			{
				pathname: '/exhibition',
				query: { id: e.id, lang: language },
			},
			toPath({
				entities,
				path: 'exhibition',
				id: e.id,
				lang: language,
			}),
		)

	let className = 'exhibition-item'
	if (isLink) {
		className += ' clickable'
	}
	if (short) {
		className += ' short'
	}
	return (
		<Consumer>
			{ctx => (
				<article className={className} onClick={onClick(ctx)}>
					<h4>{e.name}</h4>
					<p>
						<strong>{e.year}</strong>
						{e.place && ' - ' + e.place}
					</p>
					{!short && (e.startDate || e.dateEstimate) && (
						<p>
							{(e.startDate && formatDateDEShort(e.startDate)) ||
								e.dateEstimate}
							{e.endDate && ' - ' + formatDateDEShort(e.endDate)}
						</p>
					)}

					{!short && !e.hideGallery && <MarkDown markdown={e.gallery} />}
				</article>
			)}
		</Consumer>
	)
}

export default ExhibitionItem
