import classNames from 'classnames'
import Head from 'next/head'
import * as objectFitImages from 'object-fit-images'
import React from 'react'
import { I18nString } from '../entities/entity-types'
import { FixedPageIds, Page } from '../entities/page'
import { domain, Language } from '../server/config'
import { Entities } from '../server/content'
import { $t, getPageTitle } from '../utils/string'
import { Provider } from './context'
import MainFooter from './MainFooter'
import MainHeader from './MainHeader'

if (typeof window !== 'undefined') {
	objectFitImages()
}

interface Props {
	language: Language
	entities: Entities
	page?: Page
	hideMenu?: boolean
	meta?: Meta
	path: string
}

export interface Meta {
	metaDescription: I18nString
	metaFile: I18nString
	metaKeywords: I18nString
	metaTitle: I18nString
}

const defaultMeta = {
	metaTitle: 'Gunter Langer - Malerei & Grafik',
	metaDescription: 'Gunter Langer | Maler und Graphiker aus Dresden',
	metaKeywords:
		'Gunter Langer, Malerei, Grafik, Zeichnung, Kunst, Dresden, Sachsen, Deutschland',
	metaFile: '',
}

class MainContainer extends React.Component<Props> {
	render() {
		const { language, entities, page } = this.props

		const background = entities.pages[FixedPageIds.StartPage].imgs[0]
		const pageTitle = page && getPageTitle(page, language)
		const meta = this.props.meta || {
			...defaultMeta,
			metaTitle: defaultMeta.metaTitle + (pageTitle ? ' - ' + pageTitle : ''),
			metaFile: (page && page.imgs.length > 0 && page.imgs[0]) || '',
		}
		const metaFile = meta.metaFile ? domain + meta.metaFile : ''

		return (
			<Provider value={{ ...entities, language }}>
				<div
					className={classNames('main-container', {
						'transparent-background': this.props.hideMenu,
					})}
				>
					<style>{`
					body {
						background-image: url('${background}');
					}
				`}</style>
					<Head>
						<title>{$t(meta.metaTitle, language)}</title>

						<meta
							name="viewport"
							content="width=device-width, initial-scale=1.0"
						/>
						<meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />

						<meta name="title" content={$t(meta.metaTitle, language)} />
						<meta name="author" content="Gunter Langer" />
						<meta name="copyright" content="Gunter Langer" />
						<meta name="keywords" content={$t(meta.metaKeywords, language)} />
						<meta
							name="description"
							content={$t(meta.metaDescription, language)}
						/>

						<meta name="robots" content="index,follow" />
						<meta name="distribution" content="global" />
						<meta name="language" content={language} />

						<meta property="og:title" content={$t(meta.metaTitle, language)} />
						<meta property="og:type" content="website" />
						<meta property="og:url" content={domain + this.props.path} />
						<meta property="og:image" content={$t(metaFile, language)} />
						<meta
							property="og:description"
							content={$t(meta.metaDescription, language)}
						/>
						<meta property="og:site_name" content="Gunter Langer" />

						<link rel="stylesheet" href="/static/styles.css" />
						<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,Array.prototype.findIndex" />
						{/* <!-- Google Analytics--> */}
						{/* <!-- Opt-Out-Cookie--> */}
						<script
							dangerouslySetInnerHTML={{
								__html: `
							var gaProperty = 'UA-121716302-1';
							var disableStr = 'ga-disable-' + gaProperty;
							if (document.cookie.indexOf(disableStr + '=true') > -1) {
								window[disableStr] = true;
							}
							function gaOptout() {
								document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
								window[disableStr] = true;
							}
						`,
							}}
						/>
						{/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
						<script
							async
							src="https://www.googletagmanager.com/gtag/js?id=UA-121716302-1"
						/>
						<script
							dangerouslySetInnerHTML={{
								__html: `
							window.dataLayer = window.dataLayer || [];
							function gtag(){dataLayer.push(arguments);}
							gtag('js', new Date());

							gtag('config',  'UA-121716302-1', { 'anonymize_ip': true });
						`,
							}}
						/>
					</Head>

					<div className="content-wrapper">
						<MainHeader
							currentPage={this.props.page}
							hideMenu={this.props.hideMenu}
						/>
						<main>{this.props.children}</main>

						<MainFooter />
					</div>
				</div>
			</Provider>
		)
	}
}

export default MainContainer
