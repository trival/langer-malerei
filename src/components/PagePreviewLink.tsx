import cn from 'classnames'
import React from 'react'
import Slick from 'react-slick'
import { Page } from '../entities/page'
import { Language } from '../server/config'
import { getPageTitle } from '../utils/string'
import Link from './Link'
import Thumb from './Thumbnail'

interface Props {
	page: Page
	language: Language
	big?: boolean
	imgs?: string[]
}

const PagePreviewLink: React.StatelessComponent<Props> = ({
	page,
	big,
	imgs,
	language,
}) => {
	const img =
		language !== Language.DE && page.imgs.length > 1
			? page.imgs[1]
			: page.imgs[0]

	return (
		<Link path="page" id={page.id}>
			<a className={cn('preview-link', 'page-preview', { big })}>
				<figure>
					<div className="image">
						<img src={img} alt={getPageTitle(page, language)} />
					</div>
					{imgs && imgs.length > 0 && (
						<Slick
							className="preview-slider"
							arrows={false}
							autoplaySpeed={5000 + Math.random() * 4000}
							autoplay
							fade
						>
							{imgs.map((img, i) => (
								<div key={'fufu' + i}>
									<Thumb src={img} />
								</div>
							))}
						</Slick>
					)}
				</figure>
			</a>
		</Link>
	)
}

export default PagePreviewLink
