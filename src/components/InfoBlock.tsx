import cn from 'classnames'
import * as React from 'react'
import { Page } from '../entities/page'
import { $t } from '../utils/string'
import { isImage } from '../utils/url'
import { Consumer } from './context'
import Link from './Link'
import Markdown from './MarkdownContent'
import Thumb from './Thumbnail'

interface Props {
	page: Page
}

const InfoBlock: React.StatelessComponent<Props> = ({ page }) => {
	return (
		<Consumer>
			{ctx => {
				const { language: lang } = ctx

				const gallery: any[] = []
				let index = 0

				page.imgs.forEach(img => {
					if (isImage(img)) {
						gallery.push({
							id: 'p' + index,
							img,
						})
						index++
					} else {
						gallery.push({
							document: img,
						})
					}
				})

				page.articles.forEach(workId => {
					const work = ctx.articles[workId]
					if (work) {
						gallery.push({
							id: work.id,
							img: work.imgPreview,
							work,
						})
					}
				})

				const isSingle = gallery.length === 1

				return (
					<article className="info-block">
						{$t(page.title, lang) && (
							<header>
								<h3>{$t(page.title, lang)}</h3>
							</header>
						)}

						{gallery.length > 0 && (
							<section
								className={cn('block-attachments', {
									'single-attachment': isSingle,
									collection: !isSingle,
								})}
							>
								{gallery.map((item, i) =>
									item.document ? (
										<a
											href="item.document"
											target="_blank"
											className="preview-link work-preview"
											key={'doc' + i}
										>
											<Thumb src="/static/imgs/document.png" />
										</a>
									) : (
										<Link
											id={item.id}
											path="artwork"
											query={{ page: page.id }}
											key={'link' + i}
										>
											<a className="preview-link work-preview">
												<Thumb
													src={item.img}
													alt={item.work && $t(item.work.metaTitle, lang)}
												/>
											</a>
										</Link>
									),
								)}
							</section>
						)}

						<section className="block-content">
							<Markdown markdown={$t(page.text, lang)} />
						</section>
						<hr />
					</article>
				)
			}}
		</Consumer>
	)
}

export default InfoBlock
