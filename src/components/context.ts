import React from 'react'
import { Language } from '../server/config'
import { Entities } from '../server/content'

export type Context = Entities & {
	language: Language
}

export const { Consumer, Provider } = React.createContext({
	articles: {},
	pages: {},
	exhibitions: {},
	translations: {},
	language: Language.DE,
} as Context)
