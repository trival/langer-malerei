import * as React from 'react'
import { FixedPageIds, Page } from '../entities/page'
import { TranslationIds } from '../entities/translation'
import { Language } from '../server/config'
import { $t } from '../utils/string'
import { Consumer, Context } from './context'
import { Bars } from './icons/Bars'
import { Close } from './icons/Close'
import Link from './Link'

interface Props {
	currentPage?: Page
	hideMenu?: boolean
}

interface State {
	open: boolean
	isVerticalLayout: boolean
}

interface MenuItemProps {
	selected: boolean
	id: string
	label: string
	lang: string
}

const MenuItem: React.StatelessComponent<MenuItemProps> = props => (
	<li>
		<Link path="page" id={props.id}>
			<a className={props.selected ? 'selected' : ''}>{props.label}</a>
		</Link>
	</li>
)

class MainHeader extends React.Component<Props & { ctx: Context }, State> {
	window = typeof window !== 'undefined' ? window : null

	constructor(props) {
		super(props)
		this.state = {
			open: false,
			isVerticalLayout: !!(this.window && this.window.innerWidth < 760),
		}
	}

	widthListener = () => {
		const isVerticalLayout = !!(this.window && this.window.innerWidth < 760)
		if (isVerticalLayout !== this.state.isVerticalLayout) {
			this.setState(state => ({ ...state, isVerticalLayout }))
		}
	}

	componentDidMount() {
		this.widthListener()
		this.window && this.window.addEventListener('resize', this.widthListener)
	}

	componentWillUnmount() {
		this.window && this.window.removeEventListener('resize', this.widthListener)
	}

	createMenu() {
		const { language: lang, pages } = this.props.ctx
		const startPage = pages[FixedPageIds.StartPage]
		const currentPageId = this.props.currentPage && this.props.currentPage.id

		return startPage.children
			.map(pid => pages[pid])
			.map((page: Page) => ({
				selected: page.id === currentPageId,
				id: page.id,
				label: $t(page.name, lang),
				lang,
			}))
	}

	render() {
		const { language: lang, translations } = this.props.ctx

		const openMenu = () => this.setState(state => ({ ...state, open: true }))
		const closeMenu = () => this.setState(state => ({ ...state, open: false }))
		const navClass = 'main-menu' + (this.state.open ? ' open' : '')

		return (
			<header className="main-header">
				<Link lang={lang}>
					<a className="logo">
						<h1>
							Gunter Langer /{' '}
							{$t(translations[TranslationIds.websiteTitle], lang)}
						</h1>
						{!this.props.hideMenu && (
							<img
								className="logo-img"
								src={
									'/static/imgs/logo-' +
									(this.state.isVerticalLayout ? 'mobile.png' : 'desktop.png')
								}
							/>
						)}
					</a>
				</Link>

				{this.props.hideMenu && (
					<nav className="language-selector">
						<Link lang={Language.DE}>
							<a> DE </a>
						</Link>
						/
						<Link lang={Language.EN}>
							<a> EN </a>
						</Link>
					</nav>
				)}
				{!this.props.hideMenu && (
					<a className="mobile-controls open-menu" onClick={openMenu}>
						<Bars />
					</a>
				)}

				{!this.props.hideMenu && (
					<nav className={navClass}>
						<a className="mobile-controls close-menu" onClick={closeMenu}>
							<Close />
						</a>
						<ul>
							{this.createMenu().map(props => (
								<MenuItem key={props.id} {...props} />
							))}
						</ul>
					</nav>
				)}
			</header>
		)
	}
}

const MainHeaderWrapper: React.FunctionComponent<Props> = props => (
	<Consumer>{ctx => <MainHeader ctx={ctx} {...props} />}</Consumer>
)

export default MainHeaderWrapper
