import React from 'react'
import { Page } from '../entities/page'
import { Consumer } from './context'
import ExhibitionItem from './Exhibition'
import PageStandard from './PageStandard'

interface Props {
	page: Page
}

const PageExhibition: React.StatelessComponent<Props> = props => (
	<Consumer>
		{ctx => {
			const lang = ctx.language
			const p = props.page

			const exhibitions = Object.values(ctx.exhibitions)
				.filter(e => e.public)
				.sort((e1, e2) => {
					const s1 = e1.year + (e1.startDate || 9999)
					const s2 = e2.year + (e2.startDate || 9999)
					return s1.localeCompare(s2)
				})
				.reverse()

			return (
				<PageStandard page={p} language={lang}>
					{exhibitions.map(e => (
						<ExhibitionItem exhibition={e} key={'e' + e.id} language={lang} />
					))}
				</PageStandard>
			)
		}}
	</Consumer>
)

export default PageExhibition
