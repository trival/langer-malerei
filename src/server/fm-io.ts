import request from 'request'
import { Entity, Spec } from '../entities/entity-types'
import * as data from '../utils/fm-data'
import * as xml from '../utils/xml'
import * as config from './config'

function req(url: string, opt: any = {}) {
	return new Promise<string>((res, rej) => {
		request({ timeout: 1000 * 60 * 5, ...opt, url }, (err, _res, body) => {
			if (err) rej(err)
			else res(body)
		})
	})
}

// for testing purposes
export const libs = { request, xml, data }

const fm = config.filemaker

function sleep(ms = 10) {
	return new Promise((res) => setTimeout(res, ms))
}

export function transformFromFMResult<T extends Entity>(
	spec: Spec<T>,
	results: data.FMResults,
): T[] {
	return results.results.map((result) =>
		data.transform(result, spec.transformSpec),
	)
}

export function getAllRemoteXMLData<T extends Entity>(
	spec: Spec<T>,
	max = 200,
	skip = 0,
): Promise<data.FMResults> {
	let url = fm.xmlURL + fm.paramLayout + spec.fmLayoutName + fm.paramFindAll
	url += fm.paramMax + max
	url += fm.paramSkip + skip

	return req(url, { auth: fm.auth.public })
		.then(xml.parseXML)
		.then(data.getFmResults)
}

// TODO: write unit test
export async function collectAllData<T extends Entity>(
	spec: Spec<T>,
	max = 400,
): Promise<data.FMResults> {
	const results: data.FMResults = {
		currentSize: 0,
		totalSize: 1,
		results: [],
	}

	console.log('==== fetching data from', spec.fmLayoutName)
	while (results.currentSize < results.totalSize) {
		await sleep(300)
		const tempResults = await getAllRemoteXMLData(
			spec,
			max,
			results.currentSize,
		)
		console.log('new result', tempResults.currentSize)
		results.currentSize += tempResults.currentSize
		results.totalSize = tempResults.totalSize
		results.results = results.results.concat(tempResults.results)
		console.log('current result', results.currentSize)
	}

	return results
}

export function getSingleRemoteXMLData<T extends Entity>(
	spec: Spec<T>,
	id: string,
): Promise<data.FMResults> {
	const fmId = spec.transformSpec.id
	const query = fmId + '=' + id
	return getRemoteXMLDataByQuery(spec, query)
}

export function getRemoteXMLDataByQuery<T extends Entity>(
	spec: Spec<T>,
	query: string,
): Promise<data.FMResults> {
	const url =
		fm.xmlURL + fm.paramLayout + spec.fmLayoutName + fm.paramFindByQuery + query

	return req(url, { auth: fm.auth.public })
		.then(xml.parseXML)
		.then(data.getFmResults)
}
