import 'isomorphic-unfetch'
import { Language } from './config'
import { Entities } from './content'

export async function getEntityProps(initialPropArgs): Promise<Entities> {
	if (initialPropArgs && initialPropArgs.res) {
		return initialPropArgs.res.entities
	} else {
		const entities = (window as any).gl_entities as Entities

		if (!entities) {
			const newEntities = await fetch('/getEntities').then(res => res.json())
			;(window as any).gl_entities = newEntities
			return newEntities
		} else {
			return entities
		}
	}
}

export function getLanguage(initialPropArgs): Language {
	return (
		(initialPropArgs && initialPropArgs.query && initialPropArgs.query.lang) ||
		Language.DE
	)
}
