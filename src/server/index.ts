import Koa from 'koa'
import Router from 'koa-router'
import moment from 'moment'
import next from 'next'
import request from 'request'
import { clearTimeout } from 'timers'
import * as config from './config'
import * as content from './content'
import { createS3Service } from './s3service'
import { Readable } from 'stream'

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const idle = (ms = 1000) =>
	new Promise((res) => {
		setTimeout(res, ms)
	})

function createErrorLogger(text: string) {
	return (e: Error) => {
		console.error('=== catched error ' + text + ' ===')
		console.error(e.name, e.message)
		console.error(e.stack)
	}
}

let timeout

function reloadDaily() {
	console.log('Daily Reload!')
	content.reloadAll(true).catch(createErrorLogger('on reloadAll'))
	clearTimeout(timeout)
	const nextTime = moment()
		.add(1, 'day')
		.endOf('day')
		.add(6, 'hours')
		.toDate()
		.getTime()
	timeout = setTimeout(reloadDaily, nextTime - Date.now())
}

app.prepare().then(async () => {
	const koa = new Koa()
	const router = new Router()
	const assets = new Router({
		prefix: config.assetPath,
	})
	const fileService = createS3Service()

	koa.use(async (ctx, next) => {
		;(ctx.res as any).entities = await content
			.getEntities()
			.catch(createErrorLogger('on getEntities'))
		;(ctx.res as any).timestamp = content.getTimestamp()

		ctx.res.statusCode = 200
		await next()
	})

	router.get('/getEntities', async (ctx) => {
		ctx.body = (ctx.res as any).entities
	})

	router.get('/timestamp', async (ctx) => {
		ctx.body = (ctx.res as any).timestamp
	})

	router.get('/reload', async (ctx) => {
		console.log('=== Reload requested! ===')
		await idle(100)
		await content.reloadAll().catch(createErrorLogger('on reload'))

		ctx.redirect('/')
	})

	router.get('/reloadComplete', async (ctx) => {
		console.log('=== Complete reload requested! ===')
		await idle(100)
		await content.reloadAll(true).catch(createErrorLogger('on reloadComplete'))

		ctx.redirect('/')
	})

	router.post('/reload', async (ctx) => {
		console.log('=== Reload requested by post request! ===')
		await idle(100)
		await content.reloadAll().catch(createErrorLogger('on reload'))

		ctx.body = 'OK, data reloaded'
	})

	router.get('/' + config.Language.DE, async (ctx) => {
		await handle(ctx.req, ctx.res, {
			pathname: '/',
			query: { lang: config.Language.DE },
		} as any)
		ctx.respond = false
	})

	router.get('/' + config.Language.EN, async (ctx) => {
		await handle(ctx.req, ctx.res, {
			pathname: '/',
			query: { lang: config.Language.EN },
		} as any)
		ctx.respond = false
	})

	router.get('/:lang/page/:pageName/:pageId', async (ctx) => {
		await handle(ctx.req, ctx.res, {
			pathname: '/page',
			query: { id: ctx.params.pageId, lang: ctx.params.lang },
		} as any)
		ctx.respond = false
	})

	router.get(
		'/:lang/page/:pageName/:pageId/artwork/:artworkSlug/:artworkId',
		async (ctx) => {
			await handle(ctx.req, ctx.res, {
				pathname: '/artwork',
				query: {
					id: ctx.params.artworkId,
					page: ctx.params.pageId,
					lang: ctx.params.lang,
				},
			} as any)
			ctx.respond = false
		},
	)

	router.get(
		'/:lang/exhibition/:exhibitionName/:exhibitionId/',
		async (ctx) => {
			await handle(ctx.req, ctx.res, {
				pathname: '/exhibition',
				query: { id: ctx.params.exhibitionId, lang: ctx.params.lang },
			} as any)
			ctx.respond = false
		},
	)

	router.get(
		'/:lang/exhibition/:exhibitionName/:exhibitionId/artwork/:artworkSlug/:artworkId',
		async (ctx) => {
			await handle(ctx.req, ctx.res, {
				pathname: '/artwork',
				query: {
					id: ctx.params.artworkId,
					exhibition: ctx.params.exhibitionId,
					lang: ctx.params.lang,
				},
			} as any)
			ctx.respond = false
		},
	)

	router.get('/:lang/artwork/:artworkSlug/:artworkId', async (ctx) => {
		await handle(ctx.req, ctx.res, {
			pathname: '/artwork',
			query: { id: ctx.params.artworkId, lang: ctx.params.lang },
		} as any)
		ctx.respond = false
	})

	router.get('*', async (ctx) => {
		await handle(ctx.req, ctx.res)
		ctx.respond = false
	})

	assets.get('*', async (ctx) => {
		ctx.etag = content.getTimestamp().toString()
		if (ctx.fresh) {
			ctx.status = 304
			return
		}

		ctx.set('Cache-Control', 'public, max-age=1440')

		const aliases = content.getImgAliases()
		const fmUrl =
			aliases[ctx.url.toLowerCase()] ||
			ctx.url.replace(config.assetPath + '/', '')
		const s3Key = encodeURIComponent(fmUrl)

		try {
			await fileService.exists(s3Key)
			ctx.body = fileService.get(s3Key)
		} catch (e) {
			console.log('file not in cache: ', s3Key)
			const fileStream = request(config.filemaker.host + fmUrl).auth(
				config.filemaker.auth.public.user as string,
				config.filemaker.auth.public.pass as string,
				true,
			)
			const saveStream = new Readable({ read: () => {} })
			const sendStream = new Readable({ read: () => {} })
			fileStream.on('data', (chunk) => {
				saveStream.push(chunk)
				sendStream.push(chunk)
			})
			fileStream.on('complete', () => {
				saveStream.push(null)
				sendStream.push(null)
			})
			fileStream.on('error', (e) => {
				console.log('filemaker file not found: ', fmUrl, e)
				ctx.status = 404
				ctx.res.end('Not Found')
				saveStream.emit('error', e)
				sendStream.emit('error', e)
			})
			fileService.save(saveStream, s3Key).catch((e) => {
				console.error('saving file to s3 failed', s3Key, e)
			})
			ctx.body = sendStream
		}
	})

	koa.use(assets.routes() as any)
	koa.use(router.routes() as any)

	koa.addListener('error', (e) => {
		console.error('=== and uncaught application error occured ===')
		console.error(e)
		console.error(e && e.name)
		console.error(e && e.message)
		console.error(e && e.stack)
	})

	const port = process.env.PORT || 5000
	const server = koa.listen(port, () => {
		console.log(`> Ready on http://localhost:${port}`)
	})

	server.setTimeout(1000 * 60 * 5)

	reloadDaily()
})
