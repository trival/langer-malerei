// ===== languages =====

export enum Language {
	EN = 'en',
	DE = 'de',
}

// ===== Url constants =====

export const dataRefreshParameter = 'reload'
export const dataRefreshTemplate = 'reloadAll'

export const assetPath = '/assets'

export const domain = 'https://www.gunter-langer.de'

// ===== Filemaker data =====

export const filemaker = {
	host: 'https://langer.fmp-hosting.de/',
	xmlURL:
		'https://langer.fmp-hosting.de/fmi/xml/fmresultset.xml?-db=LangerMalerei',
	imgURL: '/fmi/xml/cnt/',
	paramLayout: '&-lay=',
	paramFindAll: '&-findall',
	paramFindByQuery: '&-find&',
	paramMax: '&-max=',
	paramSkip: '&-skip=',

	auth: {
		public: {
			user: process.env.FILEMAKER_USER,
			pass: process.env.FILEMAKER_PASSWORD,
		},
	},
}

// ===== entities configuration =====

export const dateParseFormat = 'MM.DD.YYYY'

// ===== Errors =====

export enum Errors {
	REMOTE_IO,
}
