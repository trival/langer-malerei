import * as article from '../entities/article'
// import * as workXpage from '../entities/pageXArticle'
import { Entity, Spec } from '../entities/entity-types'
import * as exhibition from '../entities/exhibition'
import * as workXexhibit from '../entities/exhibitionXArticle'
import * as page from '../entities/page'
import * as translation from '../entities/translation'
import * as io from './fm-io'
import * as fs from 'fs'
import * as path from 'path'
import moment from 'moment'

export interface Entities {
	articles: { [id: string]: article.Article }
	pages: { [id: string]: page.Page }
	exhibitions: { [id: string]: exhibition.Exhibition }
	translations: translation.Translations
}

const entities: Entities = {
	articles: {},
	pages: {},
	exhibitions: {},
	translations: {},
}

let imgAliases: { [url: string]: string } = {}

let reloadTimestamp = Date.now()

export async function getEntities(withoutReload = false) {
	if (!withoutReload && !Object.keys(entities.pages).length) {
		await reloadAll()
	}
	return entities
}

async function loadEntity<T extends Entity>(spec: Spec<T>, id: string) {
	const data = await io.getSingleRemoteXMLData(spec, id)
	return io.transformFromFMResult(spec, data)
}

async function loadEntities<T extends Entity>(spec: Spec<T>) {
	const data = await io.collectAllData(spec)
	return io.transformFromFMResult(spec, data)
}

async function loadArticlesSinceLastReload() {
	const date = moment(reloadTimestamp).subtract(1, 'day').format('MM/DD/YYYY')
	const data = await io.getRemoteXMLDataByQuery(
		article.spec,
		`bearbeitet=${date}&bearbeitet.op=gt`,
	)

	console.log(`loaded ${data.currentSize} new articles since last reload`)
	return io.transformFromFMResult(article.spec, data)
}

export async function getArticle(id: string) {
	if (!entities.articles[id]) {
		console.log('==== fetching article', id)
		const work = await loadEntity(article.spec, id)
		work.forEach((e) => (entities.articles[e.id] = e))
	}
	return entities.articles[id]
}

let currentlyReloading = false

export async function reloadAll(fullReload = false) {
	if (currentlyReloading) {
		return
	}

	const startTime = Date.now()
	currentlyReloading = true

	await restoreEntitiesBackup().catch((e) => {
		console.log('restoring from backup failed', e)
	})

	try {
		const articles = fullReload
			? await loadEntities(article.spec)
			: await loadArticlesSinceLastReload()
		const pages = await loadEntities(page.spec)
		const exhibitions = await loadEntities(exhibition.spec)
		const workXexhibits = await loadEntities(workXexhibit.spec)
		const translations = await loadEntities(translation.spec)

		if (fullReload) {
			reset()
		}

		articles.forEach((e) => {
			entities.articles[e.id] = e
			if (e.imgs?.[0]) {
				imgAliases[e.imgs[0].toLowerCase()] = e.imgAlias
				imgAliases[encodeURI(e.imgs[0]).toLowerCase()] = e.imgAlias
				imgAliases[encodeURIComponent(e.imgs[0]).toLowerCase()] = e.imgAlias
			}
		})
		exhibitions.forEach((e) => (entities.exhibitions[e.id] = e))
		pages.forEach((e) => (entities.pages[e.id] = e))
		translations.forEach((e) => (entities.translations[e.id] = e))

		const gallery1 = await loadEntity(page.spec, '5')
		const gallery2 = await loadEntity(page.spec, '5')

		if (entities.pages['5'].articles.length <= 1) {
			if (gallery1[0].articles.length <= 1) {
				entities.pages['5'] = gallery2[0]
			} else {
				entities.pages['5'] = gallery1[0]
			}
		}

		for (const e of workXexhibits) {
			const work = entities.articles[e.workId]
			const exhibition = entities.exhibitions[e.exhibitionId]

			if (work && exhibition) {
				work.exhibitions = []
				exhibition.works = []
			}
		}

		for (const e of workXexhibits) {
			const work = entities.articles[e.workId]
			const exhibition = entities.exhibitions[e.exhibitionId]

			if (work && exhibition) {
				work.exhibitions!.push(e.exhibitionId)
				exhibition.works!.push(e.workId)
			}
		}
	} catch (e) {
		currentlyReloading = false
		throw e
	}

	currentlyReloading = false
	reloadTimestamp = Date.now()

	console.log(
		`Reload finished after ${Math.floor(
			(reloadTimestamp - startTime) / 1000,
		)} seconds`,
	)

	await storeEntitiesBackup().catch((e) => {
		console.log('storing backup failed', e)
	})

	return entities
}

export function getTimestamp() {
	return reloadTimestamp
}

export function getImgAliases() {
	return imgAliases
}

export function reset() {
	entities.articles = {}
	entities.pages = {}
	entities.exhibitions = {}
	imgAliases = {}
}

const backupFileName = 'dataBackup.json'
const backupDirectoryName = 'backup'
const backupDirectory = path.resolve(__dirname, '..', '..', backupDirectoryName)

const backupFile = path.resolve(backupDirectory, backupFileName)

async function storeEntitiesBackup() {
	await fs.promises.mkdir(backupDirectory, { recursive: true })
	await fs.promises.writeFile(
		backupFile,
		JSON.stringify({
			entities,
			imgAliases,
			reloadTimestamp,
		}),
		{ encoding: 'utf8' },
	)
	console.log(`Data stored to ${backupDirectoryName}/${backupFileName}`)
}

async function restoreEntitiesBackup() {
	const dataString = await fs.promises.readFile(backupFile, {
		encoding: 'utf8',
	})
	const data: any = JSON.parse(dataString)
	entities.articles = { ...entities.articles, ...data.entities.articles }
	entities.exhibitions = {
		...entities.exhibitions,
		...data.entities.exhibitions,
	}
	entities.pages = { ...entities.pages, ...data.entities.pages }
	entities.translations = {
		...entities.translations,
		...data.entities.translations,
	}

	imgAliases = { ...imgAliases, ...data.imgAliases }
	reloadTimestamp = data.reloadTimestamp

	console.log(`Data restored from ${backupDirectoryName}/${backupFileName}`)
}
