import { Readable } from 'stream'
import { S3 } from 'aws-sdk'

export const awsS3Configuration: S3.ClientConfiguration = {
	endpoint: process.env.TDZ_AWS_ENDPOINT,
	accessKeyId: process.env.TDZ_AWS_ACCESS_KEY,
	secretAccessKey: process.env.TDZ_AWS_ACCESS_SECRET,
}

export const awsS3BucketName = process.env.TDZ_AWS_BUCKET_NAME

export const awsS3RootPath = process.env.TDZ_AWS_ASSET_ROOT_PATH

export function createS3Service() {
	if (!awsS3BucketName || !awsS3RootPath) {
		throw new Error(
			'cannot create s3 Service, configuration environment missing',
		)
	}

	const s3 = new S3(awsS3Configuration)

	function getKey(filePath: string) {
		return [awsS3RootPath, filePath].join('/')
	}

	function getObjectConfig(filePath: string, Body?: Readable): any {
		const options: any = { Key: getKey(filePath), Bucket: awsS3BucketName }
		if (Body) {
			options.Body = Body
		}
		return options
	}

	function get(filePath: string) {
		const config = getObjectConfig(filePath)
		// console.log('=== Getting File ===', config)
		return s3.getObject(config).createReadStream()
	}

	function exists(filePath: string) {
		return new Promise((resolve, reject) => {
			const config = getObjectConfig(filePath)
			s3.headObject(config, (err, data) => {
				if (err) reject(err)
				resolve(data)
			})
		})
	}

	function save(fileStream: Readable, filePath: string) {
		return new Promise((resolve, reject) => {
			const config = getObjectConfig(filePath, fileStream)
			// console.log('=== Saving File ===', config)
			s3.upload(config, (err, data) => {
				if (err) {
					reject(err)
				} else {
					resolve(data)
				}
			})
		})
	}

	function remove(filePath: string) {
		return new Promise<S3.DeleteObjectOutput>((resolve, reject) => {
			s3.deleteObject(getObjectConfig(filePath), (err, data) => {
				if (err) {
					reject(err)
				} else {
					resolve(data)
				}
			})
		})
	}

	function getAll(continuationToken?: string) {
		return new Promise<S3.ListObjectsOutput>((resolve, reject) => {
			s3.listObjects(
				{
					Bucket: awsS3BucketName!,
					Prefix: awsS3RootPath!,
					Marker: continuationToken,
				},
				(err, data) => {
					if (err) reject(err)
					else resolve(data)
				},
			)
		})
	}

	return { get, save, remove, exists, getAll }
}
