// @ts-nocheck
require('ts-node').register({
	transpileOnly: true,
	compilerOptions: {
		module: 'commonjs'
	}
});


require('./src/server/index.ts')
